
/** @page player Working with Player

The Player device server treats Gazebo in exactly the same way that it
treats real robot hardware: as a device that is a source of data and a
sink for commands.  A key advantage of this approach is that users may
mix Player @e abstract drivers with simulation drivers.  Thus, for
example, drivers such as VFH (Vector Field Histogram) and AMCL
(adapative Monte-Carlo localization), will work equally well with
simulated and real robots.  In the following sections, we describe
some basic scenarios that demonstate this interaction.

@section player_setting Setting up the Simulation

The basic steps for setting up and running a combined Player/Gazebo
simulation are as follows.

- Write the Gazebo world file.
- Start Gazebo.
- Write the corresponding Player configuration file(s).
- Start Player(s).
- Start client program(s).

Note that Gazebo must be started @e before the Player server, and that
the Player server must @e re-started whenever Gazebo is re-started.


@section player_single Example: Using a Single Robot

Consider the case of a single robot with a scanning laser
range-finder.  The following Gazebo world file snippet will create a
Pioneer2DX robot with SICK~LMS200 laser.

@verbatim
  <model:Pioneer2DX>
    <id>robot1_position</id>
    <xyz>0 0 0.40</xyz>
    <rpy>0 0 45</rpy>
    <model:SickLMS200>
      <id>robot1_laser</id>
      <xyz>0.15 0 0.20</xyz>
      <rpy>0 0 0</rpy>
    </model:SickLMS200>
  </model:Pioneer2DX>
@endverbatim

The corresponding snippet of the Player 0.5 configuration file should
look like this:

@verbatim
  driver
  (
    name "gz_position"
    provides ["position:0"]
    gz_id "robot1_position"
  )

  driver
  (
    name "gz_laser"
    provides ["laser:0"]
    gz_id "robot1_laser"
  )
@endverbatim

To run this simulation, start Gazebo with:

@verbatim
  $ gazebo <myworld>
@endverbatim 

where <tt>&lt;myworld&gt;</tt> is the name of the Gazebo world file, and start
Player with

@verbatim
  $ player -g 0 <myconfig>
@endverbatim 

where <tt>&lt;myconfig&gt;</tt> is the name of the Player configuration file.
Client programs can connect to the simulated devices devices on the
default Player port 6665.


@section player_vfh Example: Using a Single Robot with the VFH driver

Abstract devices can be mixed freely with simulated devices in the
Player server.  Thus, for example, it is possible to use the VFH
(Vector Field Histogram) driver with a simulated robot.  The following
Gazebo world file snippet will create a Pioneer2DX robot with
SICK~LMS200 laser.

@verbatim
  <model:Pioneer2DX>
    <id>robot1_position</id>
    <xyz>0 0 0.40</xyz>
    <rpy>0 0 45</rpy>
    <model:SickLMS200>
      <id>robot1_laser</id>
      <xyz>0.15 0 0.20</xyz>
      <rpy>0 0 0</rpy>
    </model:SickLMS200>
  </model:Pioneer2DX>
@endverbatim

The corresponding snippet of the Player configuration file should look
like this:

@verbatim
  driver
  (
    name "gz_position"
    provides ["position:0"]
    gz_id "robot1_position"
  )

  driver
  (
    name "gz_laser"
    provides ["laser:0"]
    gz_id "robot1_laser"
  )

  driver
  (
    name "vfh"
    provides ["position:1"]
    <vfh driver settings>
  )
@endverbatim

Note that the configuration file is exactly as per the first example;
we have simply added another device to the Player server.  The VFH
driver will use the simulated robot chassis and laser exactly as it
would a real robot chassis and laser.

To run this simulation, start Gazebo with:

@verbatim
  $ gazebo <myworld>
@endverbatim 

where <tt>&lt;myworld&gt;</tt> is the name of the Gazebo world file,
and start Player with

@verbatim
  $ player -g 0 <myconfig>
@endverbatim 

where <tt>&lt;myconfig&gt;</tt> is the name of the Player configuration file.
Client programs can connect to the server on the default Player port
6665.


@section player_multiple Example: Using Multiple Robots

There are a number of ways to work with multiple robots.  The simplest
way is to use multiple instances of the Player server (one for each
robot being simulated).  The following Gazebo world file snippet will
create a pair of Pioneer2DX robots with SICK~LMS200 lasers.

@verbatim
  <model:Pioneer2DX>
    <id>robot1_position</id>
    <xyz>0 0 0.40</xyz>
    <rpy>0 0 45</rpy>
    <model:SickLMS200>
      <id>robot1_laser</id>
      <xyz>0.15 0 0.20</xyz>
      <rpy>0 0 0</rpy>
    </model:SickLMS200>
  </model:Pioneer2DX>

  <model:Pioneer2DX>
    <id>robot2_position</id>
    <xyz>0 0 0.40</xyz>
    <rpy>0 0 45</rpy>
    <model:SickLMS200>
      <id>robot2_laser</id>
      <xyz>0.15 0 0.20</xyz>
      <rpy>0 0 0</rpy>
    </model:SickLMS200>
  </model:Pioneer2DX>
@endverbatim

Since there will be two instances of the Player server, two different
configuration files are required.   For the first robot:

@verbatim
  driver
  (
    name "gz_position"
    provides ["position:0"]
    gz_id "robot1_position"
  )

  driver
  (
    name "gz_laser"
    provides ["laser:0"]
    gz_id "robot1_laser"
  )
@endverbatim

and for the second robot:

@verbatim
  driver
  (
    name "gz_position"
    provides ["position:0"]
    gz_id "robot2_position"
  )

  driver
  (
    name "gz_laser"
    provides ["laser:0"]
    gz_id "robot2_laser"
  )
@endverbatim

Note that these files are identical apart from the <tt>gz_id</tt>
property.  In general, however, the simulated robots may be
heterogeneous, in which case the Player configuration files will
differ substantially.

To run this simulation, start Gazebo with:

@verbatim
  $ gazebo <myworld>
@endverbatim 

where <tt>&lt;myworld&gt;</tt> is the name of the Gazebo world file,
and start two instances of Player with

@verbatim
  $ player -p 7000 -g 0 <myconfig1>
@endverbatim 

and

@verbatim
  $ player -p 7001 -g 0 <myconfig2>
@endverbatim 

where <tt>&lt;myconfig1&gt;</tt> and <tt>&lt;myconfig2&gt;</tt> are
the two Player configuration files.  Client programs can connect to
the robots <tt>robot1</tt> and <tt>robot2</tt> through ports 7000 and
7001, respectively.



@section player_prefix Example: Using Multiple Robots with --gazebo-prefix

When the simulated robots are homogeneous, one may simplify the
process somewhat by employing the <tt>--gazebo-prefix</tt> flag with
Player.  The following Gazebo world file snippet will create a pair of
Pioneer2DX robots with SICK~LMS200 lasers.

@verbatim
  <model:Pioneer2DX>
    <id>robot1_position</id>
    <xyz>0 0 0.40</xyz>
    <rpy>0 0 45</rpy>
    <model:SickLMS200>
      <id>robot1_laser</id>
      <xyz>0.15 0 0.20</xyz>
      <rpy>0 0 0</rpy>
    </model:SickLMS200>
  </model:Pioneer2DX>

  <model:Pioneer2DX>
    <id>robot2_position</id>
    <xyz>0 0 0.40</xyz>
    <rpy>0 0 45</rpy>
    <model:SickLMS200>
      <id>robot2_laser</id>
      <xyz>0.15 0 0.20</xyz>
      <rpy>0 0 0</rpy>
    </model:SickLMS200>
  </model:Pioneer2DX>
@endverbatim

Since the robots are identical, we can write one Player configuration
file for both:

@verbatim
  driver
  (
    name "gz_position"
    provides ["position:0"]
    gz_id "_position"
  )

  driver
  (
    name "gz_laser"
    provides ["laser:0"]
    gz_id "_laser"
  )
@endverbatim

Note that the <tt>gz_id</tt> values are incomplete; we will add {\em
prefix} to these values when the Player server is started.

To run this simulation, start Gazebo with:

@verbatim
  $ gazebo <myworld>
@endverbatim 

where <tt>&lt;myworld&gt;</tt> is the name of the Gazebo world file,
and start two instances of Player with

@verbatim
  $ player -p 7000 -g 0 --gazebo-prefix robot1 <myconfig>
@endverbatim 

and

@verbatim
  $ player -p 7001 -g 0 --gazebo-prefix robot 2 <myconfig>
@endverbatim 

where <tt>&lt;myconfig&gt;</tt> is the common Player configuration file.
Client programs can connect to the robots <tt>robot1</tt> and
<tt>robot2</tt> through ports 7000 and 7001, respectively.

*/
