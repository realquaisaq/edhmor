#ifndef INIT_MAPS_HPP
#define INIT_MAPS_HPP
	
#include "libgazebo/IfaceFactory.hh"
#include "server/sensors/SensorFactory.hh"
#include "server/controllers/ControllerFactory.hh"
using namespace libgazebo;
using namespace gazebo;

std::map<std::string, IfaceFactoryFn> IfaceFactory::ifaces;
std::map<std::string, SensorFactoryFn> SensorFactory::sensors;
std::map<std::string, ControllerFactoryFn> ControllerFactory::controllers;
	
	
	
#endif
