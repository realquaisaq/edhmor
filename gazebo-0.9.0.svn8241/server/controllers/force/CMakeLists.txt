include (${gazebo_cmake_dir}/GazeboUtils.cmake)

SET (sources Force.cc)
SET (headers Force.hh)

APPEND_TO_SERVER_SOURCES(${sources})
APPEND_TO_SERVER_HEADERS(${headers})

