/*
 *  Gazebo - Outdoor Multi-Robot Simulator
 *  Copyright (C) 2003  
 *     Nate Koenig & Andrew Howard
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
/* Desc: Generic Gazebo Device Inteface for Player
 * Author: Nate Koenig
 * Date: 2 March 2006
 * CVS: $Id: GazeboInterface.hh 7043 2008-09-24 18:11:12Z natepak $
 */

#ifndef GAZEBOINTERFACE_HH
#define GAZEBOINTERFACE_HH

#include <libplayercore/playercore.h>

namespace gazebo
{

/// \addtogroup player
/// \brief Base class for all the player interfaces
/// \{

// Forward declarations
class GazeboDriver;

/// \brief Base class for all the player interfaces
class GazeboInterface
{
  /// \brief Constructor
  public: GazeboInterface(player_devaddr_t addr, GazeboDriver *driver,
                    ConfigFile *cf, int section);

  /// \brief Destructor
  public: virtual ~GazeboInterface();

  /// \brief Handle all messages. This is called from GazeboDriver
  public: virtual int ProcessMessage(QueuePointer &respQueue,
                                     player_msghdr_t *hdr, void *data) = 0;

  /// \brief Update this interface, publish new info.
  public: virtual void Update() = 0;

  /// \brief Open a SHM interface when a subscription is received.
  ///        This is called fromGazeboDriver::Subscribe
  public: virtual void Subscribe() = 0;

  /// \brief Close a SHM interface. This is called from
  ///        GazeboDriver::Unsubscribe
  public: virtual void Unsubscribe() = 0;

  /// \brief Address of the Player Device
  public: player_devaddr_t device_addr;

  /// \brief Driver instance that created this device
  public: GazeboDriver *driver;
};

/// \}


}
#endif
