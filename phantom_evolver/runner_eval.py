from sim_manager import PhantomVrepController
import sys
from evo_tools import evaluator
from analysis_tools import get_chromo
from deap import tools, creator, base

if(len(sys.argv) != 4):
	print("Requires two argument that is the chromosome, timeout, and scene")

chromo = get_chromo(sys.argv[1])
timeout = float(sys.argv[2])
scene = sys.argv[3]

controller = PhantomVrepController(0.05, 19997, 18, scene)
print("Result:", evaluator(chromo, lambda : controller, timeout, allow_collisions=True))