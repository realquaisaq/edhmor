from os import path
import subprocess
import itertools

best_folder = "best/"

if not path.exists(best_folder):
	print("Requires best folder")

sims = ["ode", "bullet", "newton"]

for i in range(1, 21):
	for (sim1, sim2) in itertools.combinations(sims, 2):
		file = best_folder + "base_%s_%s.out" % (sim1, i)
		if not path.exists(file):
			print("Skipping", file)
		else:
			target_file = best_folder + "base_%s_%s_%s.out" % (sim1, sim2, i)
			subprocess.call("python .\\runner_eval_transf.py %s 15 100 phantom/%s_50ms_06fric.ttt phantom/%s_50ms_06fric.ttt %s" % (file, sim1, sim2, target_file), shell=True)