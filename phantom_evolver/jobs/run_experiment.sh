#!/bin/bash

if ["${PHANTOM_RESUME}" -eq ""]; then
	c_folder="checkpoints_${1}/${SLURM_JOB_ID}"
else
	c_folder=$PHANTOM_RESUME
fi
mkdir -p $c_folder

echo Host: $SLURM_JOB_NODELIST
echo CPUs: $SLURM_NTASKS
echo SLURM_ARRAY_TASK_ID: $SLURM_ARRAY_TASK_ID

tasks=$((2*$SLURM_NTASKS))
echo Number of tasks: $tasks

echo Placing checkpoints in: $c_folder
log="${c_folder}/log.out"
best="${c_folder}/best.out"

tries=10
port=45576

echo Upon fail I will call sbatch --export=ALL,PHANTOM_RESUME=$c_folder $1.job

for try in $(seq 0 $tries); do
	cmd="python3 -u ${2} -l ${log} -b ${best} -f ${c_folder} -H -c ${tasks} -S ../vrep.sif --resume -P ${port}"
	echo Running command:
	echo $cmd
	$cmd
	fail=$?
	if [ $fail -eq 0 ]; then
		echo Completed task
		break
	elif [ $fail -eq 5 ]; then
		let port=port+tasks+1
  		echo Failed attempt nr. $try
		if [ $try -eq $tries ]; then
			sbatch --export=ALL,PHANTOM_RESUME=$c_folder $1.job
			exit 1
		fi
	else
		echo Failed with code $fail
		break
	fi
done
