experiment_folder="checkpoints_${1}"

for c_folder in $experiment_folder/*/ ; do
	if [ ! -f "${c_folder}/best.out" ]; then
		sbatch --export=ALL,PHANTOM_RESUME=$c_folder $1.job
	fi
done