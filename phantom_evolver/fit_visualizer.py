from deap.tools import Logbook
from deap import creator, base
import matplotlib.pyplot as plt
import pickle
import sys
import glob
import statistics
import numpy as np

if(len(sys.argv) < 3):
	print("Requires at least one argument that is the logbook file")

#creator.create("FitnessMax", base.Fitness, weights=(0,)) # dummy fitness
#creator.create("Metrics", dict)
#creator.create("Individual", list, fitness=creator.FitnessMax, metrics=creator.Metrics)

fig, ax = plt.subplots(1)

def get_gen_max(file):
	print("Opening", file)
	with open(file, "rb") as cp_file:
		logbook = pickle.load(cp_file)
	return logbook.select("gen", "max")

def ind_plot(i):
	file = sys.argv[i]
	if ":" in file:
		file = file.split(":")[0]
		label = file.split(":")[1]
	else:
		label = file
	for f in glob.glob(file):
		gen, max = get_gen_max(f)
		ax.plot(gen, max, label=f)
	return i+1

def median_plot(i):
	maxes = []
	files = []
	file = sys.argv[i]
	while not file.startswith("]"):
		if ":" in file:
			files.append(file.split(":")[1])
			file = file.split(":")[0]
		else:
			files.append(file)
		for f in glob.glob(file):
			gen, max = get_gen_max(f)
			maxes.append(max)
		i += 1
		file = sys.argv[i]
	if(":" in file):
		label = file.split(":")[1]
	else:
		if(len(files) == 1):
			label = files[0]
		else:
			label = str(files)
	med = list(map(lambda a: statistics.median(a), zip(*maxes)))
	seventyfive = list(map(lambda a: np.percentile(a, 75), zip(*maxes)))
	twentyfive = list(map(lambda a: np.percentile(a, 25), zip(*maxes)))
	xs = list(range(0, len(med)))
	#ax.fill_between(xs, seventyfive, twentyfive, alpha=0.5)
	ax.plot(xs, med, label=label)
	return i+1

i = 2
while i < len(sys.argv):
	file = sys.argv[i]
	if(file == "["):
		i = median_plot(i+1)
	else:
		i = ind_plot(i)

plt.legend(bbox_to_anchor=(1, 1), loc='upper left')
ax.set_ylabel("Fitness")
ax.set_xlabel("Generation")
ax.set_title(sys.argv[1])
ax.set_aspect(1 / ax.get_data_ratio())
plt.tight_layout(rect=[0,0,1,1])
plt.show()