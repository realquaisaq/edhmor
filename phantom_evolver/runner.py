from sim_manager import PhantomVrepController
import sys

if(len(sys.argv) != 3):
	print("Requires one argument that is the chromosome and scene")

chromo_str = sys.argv[1]
chromo_str = chromo_str[1:] # Get rid of starting '['
chromo_str = chromo_str[:-1] # Get rid of trailing ']'
print(chromo_str.split(", "))
chromo = [float(i) for i in chromo_str.split(", ")]
print(chromo)

controller = PhantomVrepController(0.05, 19997, 18, sys.argv[2])
controller.set_chromo(chromo)
controller.prompt_lifecycle(timeout=20)
controller.close()