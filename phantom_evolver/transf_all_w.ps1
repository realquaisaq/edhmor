param(
	[string]$path = "best"
)
Get-ChildItem $path -Filter *.out | 
Foreach-Object {
	$file="$($path)/$($_.Name)"
	echo "Evaluating $($file)"
	$filename = [io.path]::GetFileNameWithoutExtension($file)
	python .\transf_eval.py $file "rl/$($filename)_dp_1.txt" "rl/$($filename)_dp_2.txt" "rl/$($filename)_dp_3.txt"
}