#!/bin/bash

if [ $# -ne 2 ]; then
    echo "No enough arguments provided"
    exit 1
fi

FILE=results/logs/main_bullet_$1.out
if [ -f "$FILE" ]; then
    echo "Given execution nr. already exists"
    exit 1
fi

declare -a experiments=("main_bullet_ode" "main_bullet_newton" "main_ode_newton" "main_ode_bullet" "main_newton_bullet" "main_newton_ode")

for e in "${experiments[@]}"
do
   if [ $2 == "m" ] || [ $2 == "a" ]; then
      mv checkpoints_$e/log.out "results/logs/${e}_${1}.out"
      mv checkpoints_$e/best.out "results/best/${e}_${1}.out"
      rm checkpoints_$e/checkpoint_*
   fi
   if [ $2 == "r" ] || [ $2 == "a" ]; then
      sbatch --array=0-11%1 $e.job
   fi
done
