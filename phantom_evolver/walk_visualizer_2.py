import matplotlib.pyplot as plt
import pickle
import sys
import numpy as np
import glob
import os
from analysis_tools import get_chromo, create_walk_graph
from evo_tools import distance

if(len(sys.argv) < 2):
	print("Requires at least one argument that is the logbook file")


for i in range(1, len(sys.argv)):
	file = sys.argv[i]
	for f in glob.glob(file):
		ind = get_chromo(f)
		pos_and_label.append((ind.metrics["walk1"], f + " 1"))
		pos_and_label.append((ind.metrics["walk2"], f + " 2"))
		
create_walk_graph(*pos_and_label)
plt.show()