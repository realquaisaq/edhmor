import pickle
import sys
import os
from evo_tools import distance
from statistics import mean, median
from math import atan2, pi
from analysis_tools import split_walk_results, fix_walk
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import numpy as np
from scipy.stats import mannwhitneyu

def origo_dist(pos):
	return distance(pos, (0,0))

def angle_between(previous, current, next):
	relative_previous = (previous[0]-current[0], previous[1]-current[1])
	relative_next = (next[0]-current[0], next[1]-current[1])
	angle1 = atan2(relative_previous[0], relative_previous[1])
	angle1 = (angle1 + 2*pi) % (2*pi)
	angle2 = atan2(relative_next[0], relative_next[1])
	angle2 = (angle2 + 2*pi) % (2*pi)
	diff = angle1 - angle2
	return diff

def angle_between_origo_and_next(current, next):
	return angle_between((0,0), current, next)

def smallest_angle_difference(angle_diff, signed=False):
	diff = abs(angle_diff) % (2*pi)
	if diff > pi:
		diff = 2*pi - diff
	if signed and angle_diff < 0:
		diff = -diff
	return diff

def preprocess_walks(l1, l2, n=6):
	if len(l1) != len(l2):
		l_len = min(len(l1), len(l2))
		if l_len < n:
			n = l_len
	def preprocess_walk(l):
		x_chunks = np.array_split(list(zip(*l))[0], n)
		y_chunks = np.array_split(list(zip(*l))[1], n)
		x = [mean(xs) for xs in x_chunks]
		y = [mean(ys) for ys in y_chunks]
		return list(zip(x, y))
	return (preprocess_walk(l1), preprocess_walk(l2))

def shape_diff(walk1, walk2):
	walk1, walk2 = preprocess_walks(walk1, walk2)
	differences = []
	walks = list(zip(walk1, walk2))
	for i, (pos1, pos2) in enumerate(walks[1:-1], start=1):
		angle1 = angle_between(walk1[i-1], pos1, walk1[i+1])
		angle2 = angle_between(walk2[i-1], pos2, walk2[i+1])
		diff = smallest_angle_difference(angle1 - angle2, signed=False)
		differences.append(diff)
		#print(pos1, pos2, round(angle1, 2), round(angle2, 2), round(diff, 2), sep="\t")
	return abs(sum(differences))

def disparity(walk1, walk2):
	walk1, walk2 = preprocess_walks(walk1, walk2, n=30)
	walk1_distances = [origo_dist(p) for p in walk1]
	walk1_avg_d = mean(walk1_distances)
	walk2_distances = [origo_dist(p) for p in walk2]
	walk2_avg_d = mean(walk2_distances)
	return sum([((S-R)**2)/(walk2_avg_d*walk1_avg_d) for S,R in zip(walk2_distances,walk1_distances)])

def orientation_diff(walk1, walk2):
	walk1_xs, walk1_ys = zip(*walk1)
	mean1 = (mean(walk1_xs), mean(walk1_ys))
	walk2_xs, walk2_ys = zip(*walk2)
	mean2 = (mean(walk2_xs), mean(walk2_ys))
	#angle = angle_between(walk1[-1], (0,0), walk2[-1])
	angle = angle_between(mean1, (0,0), mean2)
	return smallest_angle_difference(angle, signed=False)

def fitness_diff(walk1, walk2):
	return abs(origo_dist(walk1[-1]) - origo_dist(walk2[-1]))

#print("name", "shape_diff", "fitness_diff", "orientation_diff", sep="\t\t")

def do_everything(func, name, boxplots=True, density=True):
	exclude = ["base_bullet_inc_16260", "transf_all_17924", "transf_newton_bullet_ode_17713", "base_newton_inc_16334", "main_newton_bullet_17478"]
	Ds = {}
	for filename in os.listdir("walks"):
		path = "walks/" + filename
		experiment_name = filename.split(".")[0]
		if(experiment_name in exclude):
			continue
		sim = experiment_name.split("_")[1]
		phase = experiment_name.split("_")[0]
		if phase == "main":
			sim2 = experiment_name.split("_")[2]
		elif phase == "base":
			sim2 = sim
		elif phase == "transf":
			if sim == "all":
				sim = "random"
			sim2 = "random"
		else:
			raise Exception("Unknown phase")
		with open(path, "rb") as f:
			Ds_for_this = {"orientation": [], "fitness":[], "shape":[], "transferability":[]}
			all_results = pickle.load(f)
			sim_results, rl_results = split_walk_results(all_results)
			target_reality = func((rl_results, sim_results))
			#for _,rl_walk in rl_results:
			#	fix_walk(rl_walk)
			for _,rl_walk in target_reality:
				for label,sim_walk in sim_results:
					if label.startswith(sim) or sim == "random":
						#print(experiment_name, shape_diff(rl_walk, sim_walk), fitness_diff(rl_walk, sim_walk), , sep="\t\t")
						Ds_for_this["orientation"].append(orientation_diff(rl_walk, sim_walk))
						Ds_for_this["fitness"].append(fitness_diff(rl_walk, sim_walk))
						Ds_for_this["shape"].append(shape_diff(rl_walk, sim_walk))
						Ds_for_this["transferability"].append(disparity(rl_walk, sim_walk))
			if (sim,sim2, "orientation") in Ds:
				Ds[(sim,sim2, "orientation")].append(mean(Ds_for_this["orientation"]))
			else:
				Ds[(sim,sim2, "orientation")] = [mean(Ds_for_this["orientation"])]
			if (sim,sim2, "fitness") in Ds:
				Ds[(sim,sim2, "fitness")].append(mean(Ds_for_this["fitness"]))
			else:
				Ds[(sim,sim2, "fitness")] = [mean(Ds_for_this["fitness"])]
			if (sim,sim2, "shape") in Ds:
				Ds[(sim,sim2, "shape")].append(mean(Ds_for_this["shape"]))
			else:
				Ds[(sim,sim2, "shape")] = [mean(Ds_for_this["shape"])]
			if (sim,sim2, "transferability") in Ds:
				Ds[(sim,sim2, "transferability")].append(mean(Ds_for_this["transferability"]))
			else:
				Ds[(sim,sim2, "transferability")] = [mean(Ds_for_this["transferability"])]

	sims = ["bullet", "ode", "newton", "random"]
	scores = ["fitness", "orientation", "shape", "transferability"]
	# Create libtex tables
	for score in scores:
		print("\n", score)
		print("\t", end="")
		for sim in sims:
			print("%s\t\t\t" % sim, end="")
		print("")
		for sim2 in sims:
			print("%s\t" % sim2, end="")
			for sim1 in sims:
				if sim1 == "random" and sim2 != "random":
					print("-\t")
				else:
					data = Ds[(sim1,sim2,score)]
					print("%.2f %.2f +%.2f -%.2f\t" % (mean(data), median(data), np.percentile(data, 75) - median(data), median(data) - np.percentile(data, 25)), end="")
			print("")
		print("\n\\resulttable", end="")
		for sim2 in sims:
			for sim1 in sims:
				if sim1 == "random" and sim2 != "random":
					#print("{-}")
					continue
				else:
					data = Ds[(sim1,sim2,score)]
					print("{$%.2f^{+%.2f}_{-%.2f}$}" % (median(data), np.percentile(data, 75) - median(data), median(data) - np.percentile(data, 25)), end="")
		print("")
	
	# Create Mann-Whiteney U test results
	mwu = {}
	for score in scores:
		for sim1 in sims:
			if sim1 == "random" or sim1 == name:
				continue
			control = Ds[(sim1, sim1, score)]
			for sim2 in sims:
				if sim1 != sim2:
					mwu[(sim1,sim2,score)] = mannwhitneyu(control, Ds[(sim1,sim2,score)], alternative="two-sided")[1]
	
	def stars(p):
		if p < 0.0001:
			return "****"
		elif (p < 0.001):
			return "***"
		elif (p < 0.01):
			return "**"
		elif (p < 0.05):
			return "*"
		else:
			return "-"
	
	if boxplots:
		annotation_map = {
			(1, 2, 1, "0.2", 'bullet', 'random'),
			(1, 3, 1.08, "0.1", 'bullet', 'newton'),
			(1, 4, 1.16, "0.08", 'bullet', 'random'),
			(5, 6, 1, "0.2", 'ode', 'bullet'),
			(6, 7, 1, "0.2", 'ode', 'newton'),
			(6, 8, 1.08, "0.1", 'ode', 'random'),
			(9, 11, 1.08, "0.1", 'newton', 'bullet'),
			(10, 11, 1, "0.2", 'newton', 'ode'),
			(11, 12, 1, "0.2", 'newton', 'random')
		}
		
		# Create boxplots
		for score in scores:
			data = []
			labels = []
			for sim1 in sims:
				for sim2 in sims:
					if sim1 == "random" and sim2 != "random" or (sim1 == "newton" and name == "newton"):
						continue
					data.append(Ds[(sim1,sim2,score)])
					labels.append(sim1 + "-" + sim2 if sim1 != sim2 or sim1 == "random" else sim1)
			fig, ax = plt.subplots(1)
			r = ax.boxplot(data, notch=False, whis=[10,90], labels=labels, patch_artist=True, widths=0.75, showfliers=False)
			r["boxes"][0].set_facecolor("#2ca02c")
			r["boxes"][5].set_facecolor("#2ca02c")
			r["boxes"][3].set_facecolor("mediumblue")
			r["boxes"][7].set_facecolor("mediumblue")
			if name != "newton":
				r["boxes"][10].set_facecolor("#2ca02c")
				r["boxes"][11].set_facecolor("mediumblue")
				r["boxes"][12].set_facecolor("mediumblue")
			else:
				r["boxes"][8].set_facecolor("mediumblue")
			plt.xticks(rotation=-45, ha='left', rotation_mode="anchor")
			#if(score == "transferability" and name == "rl"):
			#	plt.ylim(-5, 55)
			for sig_anno in annotation_map:
				if(sig_anno[4] == name):
					continue
				ax.annotate("", xy=(sig_anno[0], sig_anno[2]), xycoords=('data', 'axes fraction'),
					xytext=(sig_anno[1], sig_anno[2]), textcoords=('data', 'axes fraction'),
					arrowprops=dict(arrowstyle="-", ec='#aaaaaa',
					connectionstyle="bar,fraction=" + sig_anno[3]))
				ax.annotate(stars(mwu[(sig_anno[4],sig_anno[5],score)]), xy=(sig_anno[0], sig_anno[2]), xycoords=('data', 'axes fraction'),
					xytext=(sig_anno[0] - (sig_anno[0] - sig_anno[1]) / 2, sig_anno[2] + 0.05), textcoords=('data', 'axes fraction'),
					ha='center', va='center')
			ax.set_aspect(1/ax.get_data_ratio())
			ax.set_title("(" + {"fitness":"a", "orientation":"b", "shape":"c", "transferability":"d"}[score] + ") Error method: " + score, pad=55)
			ax.set_ylabel("Error")
			fig.tight_layout()
			plt.savefig("C:/Users/Rune/Desktop/results/score_" + score + "_" + name + ".pdf", bbox_inches = 'tight', pad_inches = 0)
	
	if(density):
		for score in scores:
			base_data = []
			main_data = []
			for sim1 in sims:
				for sim2 in sims:
					if(sim1 == "random" or sim2 == "random"):
						continue
					if(sim1 == sim2):
						base_data.extend(Ds[(sim1, sim2,score)])
					else:
						main_data.extend(Ds[(sim1, sim2,score)])
			fig, ax = plt.subplots(1)
			if score == "transferability":
				bins = np.arange(110, step=2)
			elif score == "shape":
				bins = np.arange(9.5, step=0.25)
			elif score == "fitness":
				bins = np.arange(1.3, step=0.05)
			elif score == "orientation":
				bins = np.arange(1.5, step=0.05)
			else:
				raise Exception("what?")
				#max_point = max(base_data + main_data)
				#bins = np.arange(max_point, step=0.1)
			ax.hist(main_data, bins=bins, alpha=0.5, label='experiment group', edgecolor="k", density=False, color='blue', weights=np.ones_like(main_data)/float(len(main_data)))
			ax.hist(base_data, bins=bins, alpha=0.5, label='control group', edgecolor="k", density=False, color='green', weights=np.ones_like(base_data)/float(len(base_data)))
			ax.set_title("(" + {"fitness":"a", "orientation":"b", "shape":"c", "transferability":"d"}[score] + ") Error method: " + score)
			ax.set_ylabel("Density")
			ax.set_xlabel("Error")
			ax.yaxis.set_major_formatter(mtick.PercentFormatter(1.0))
			ax.legend(loc='upper right')
			plt.savefig("C:/Users/Rune/Desktop/results/score_density_" + score + "_" + name + ".pdf", bbox_inches = 'tight', pad_inches = 0)
	
	#plt.show()

def filter_walks(sim, sim_walks):
	return [(label,walk) for (label,walk) in sim_walks if label.startswith(sim)]

if __name__ == "__main__":
	do_everything(lambda rl_sim: [(label,fix_walk(walk)) for label,walk in rl_sim[0]], "rl", False, False)
	#do_everything(lambda rl_sim: filter_walks("newton", rl_sim[1]), "newton")
	#do_everything(lambda rl_sim: filter_walks("ode", rl_sim[1]), "ode")
	#do_everything(lambda rl_sim: filter_walks("bullet", rl_sim[1]), "bullet")