from evo_tools import distance
from statistics import mean
from math import atan2, pi, asin, sqrt

def angle_between(previous, current, next):
	relative_previous = (previous[0]-current[0], previous[1]-current[1])
	relative_next = (next[0]-current[0], next[1]-current[1])
	angle1 = atan2(relative_previous[0], relative_previous[1])
	angle1 = (angle1 + 2*pi) % (2*pi)
	angle2 = atan2(relative_next[0], relative_next[1])
	angle2 = (angle2 + 2*pi) % (2*pi)
	diff = angle1 - angle2
	#rad = rad-2*pi if rad > pi else rad
	#rad = rad+2*pi if rad < -pi else rad
	return diff

def angle_between_origo_and_next(current, next):
	return angle_between((0,0), current, next)

import pdb; pdb.set_trace()

def preprocess_walks(l1, l2):
	if(len(l1) != len(l2)):
		if(len(l1) > len(l2)):
			l1 = l1[:len(l2)-1]
		else:
			l2 = l2[:len(l1)-1]
		#raise Exception("Must be same length", len(walk1), len(walk2))
	#l1 = l1[0::6]
	#l2 = l2[0::6]
	return (l1, l2)

def smallest_angle_difference(angle1, angle2, signed=False):
	diff = abs(angle1 - angle2) % (2*pi)
	if diff > pi:
		diff = 2*pi - diff
	if signed and angle1-angle2 < 0:
		diff = -diff
	return diff

def shape_similarity_only_next_abs_origo(walk1, walk2):
	walk1, walk2 = preprocess_walks(walk1, walk2)
	differences = []
	walks = list(zip(walk1, walk2))
	for i, (pos1, pos2) in enumerate(walks[1:-1], start=1):
		angle1 = angle_between_origo_and_next(pos1, walk1[i+1])
		angle2 = angle_between_origo_and_next(pos2, walk2[i+1])
		diff = smallest_angle_difference(angle1, angle2)
		differences.append(diff)
		print(pos1, pos2, round(angle1, 2), round(angle2, 2), round(diff, 2), sep="\t")
	return sum(differences)

def shape_similarity_only_next_signed_origo(walk1, walk2):
	walk1, walk2 = preprocess_walks(walk1, walk2)
	differences = []
	walks = list(zip(walk1, walk2))
	for i, (pos1, pos2) in enumerate(walks[1:-1], start=1):
		angle1 = angle_between_origo_and_next(pos1, walk1[i+1])
		angle2 = angle_between_origo_and_next(pos2, walk2[i+1])
		diff = smallest_angle_difference(angle1, angle2, signed=True)
		differences.append(diff)
		#print(pos1, pos2, round(angle1, 2), round(angle2, 2), round(diff, 2), sep="\t")
	return abs(sum(differences))

def shape_similarity_only_next_signed_previous(walk1, walk2):
	walk1, walk2 = preprocess_walks(walk1, walk2)
	differences = []
	walks = list(zip(walk1, walk2))
	for i, (pos1, pos2) in enumerate(walks[1:-1], start=1):
		angle1 = angle_between(walk1[i-1], pos1, walk1[i+1])
		angle2 = angle_between(walk1[i-1], pos2, walk2[i+1])
		diff = smallest_angle_difference(angle1, angle2, signed=True)
		differences.append(angle1 - angle2)
	return abs(sum(differences))

def shape_similarity_all_next_only_previous(walk1, walk2):
	walk1, walk2 = preprocess_walks(walk1, walk2)
	differences = []
	walks = list(zip(walk1, walk2))
	for i, (pos1, pos2) in enumerate(walks[1:-1], start=1):
		for (next1, next2) in walks[i+1:]:
			angle1 = angle_between(walk1[i-1], pos1, next1)
			angle2 = angle_between(walk2[i-1], pos2, next2)
			differences.append(smallest_angle_difference(angle1, angle2))
	return sum(differences)

def shape_similarity_all_next_all_previous(walk1, walk2):
	walk1, walk2 = preprocess_walks(walk1, walk2)
	differences = []
	walks = list(zip(walk1, walk2))
	for i, (pos1, pos2) in enumerate(walks[1:-1], start=1):
		for (next1, next2) in walks[i+1:]:
			for (previous1, previous2) in walks[:i]:
				angle1 = angle_between(previous1, pos1, next1)
				angle2 = angle_between(previous2, pos2, next2)
				differences.append(smallest_angle_difference(angle1, angle2))
	return sum(differences)

def shape_similarity_all_next_origo(walk1, walk2):
	walk1, walk2 = preprocess_walks(walk1, walk2)
	differences = []
	walks = list(zip(walk1, walk2))
	for i, (pos1, pos2) in enumerate(walks[1:-1], start=1):
		for (next1, next2) in walks[i+1:]:
			angle1 = angle_between_origo_and_next(pos1, next1)
			angle2 = angle_between_origo_and_next(pos2, next2)
			differences.append(smallest_angle_difference(angle1, angle2))
	return sum(differences)

#walk1 = [(0,0), (0,1),(1,2),(0,3),(-1,2)]
#walk2 = [(0,0), (1,0),(2,-1),(3,0),(2,1)]
walk1 = [(0,0), (1,0),(2,0),(4,0),(5,0)]
walk2 = [(0,0), (1,0),(2,0.2),(3,-0.2),(5,0)]
walk3 = [(0,0), (1,0),(2,0.2),(3,0.5),(5,1)]

print(round(shape_similarity_only_next_abs_origo(walk1, walk2), 2), round(shape_similarity_only_next_abs_origo(walk1, walk3), 2))
print(round(shape_similarity_only_next_signed_origo(walk1, walk2), 2), round(shape_similarity_only_next_signed_origo(walk1, walk3), 2))
print(round(shape_similarity_only_next_signed_previous(walk1, walk2), 2), round(shape_similarity_only_next_signed_previous(walk1, walk3), 2))
print(round(shape_similarity_all_next_only_previous(walk1, walk2), 2), round(shape_similarity_all_next_only_previous(walk1, walk3), 2))
print(round(shape_similarity_all_next_all_previous(walk1, walk2), 2), round(shape_similarity_all_next_all_previous(walk1, walk3), 2))
print(round(shape_similarity_all_next_origo(walk1, walk2), 2), round(shape_similarity_all_next_origo(walk1, walk3), 2))