from emerge import Controller, Module, print_modules
from vreppy import VRep
from vreppy.vrep import vrep as api
from vreppy.common import ReturnCommandError
from multiprocessing import current_process, Manager
import numpy as np
import subprocess
import os
import sys
import traceback
import time
import signal
import atexit

class PhantomVrepController(Controller):
	def __init__(self, timestep, port, num_modules, scene):
		self.num_modules = num_modules
		self.timestep = timestep
		self.t = None
		self.scene = scene
		self.port = port
		self.connect(port)
		code, value = api.simxGetFloatingParameter(self.vrep._id, api.sim_floatparam_simulation_time_step, api.simx_opmode_streaming)
		self.load_scene(scene)
		#if(value != timestep):
			#print("WARNING: scene timestep", value, "and controller timestep", timestep, "are different")

	def connect(self, port):
		print("Connecting to", port)
		tries = 10
		while(True):
			try:
				self.vrep = VRep.connect("127.0.0.1", port)
				break
			except ReturnCommandError as e:
				tries = tries - 1
				if(tries <= 0):
					raise VrepException("It failed to connect to vrep!")
		
		tries = 10
		while(True):
			code = api.simxSynchronous(self.vrep._id, True)
			if(code != api.simx_return_ok):
				tries = tries - 1
				if(tries <= 0):
					raise VrepException("It failed to set sync! " + str(code))
			else:
				break
		print("Successfully connected to", port)

	def load_scene(self, scene):
		code = api.simxLoadScene(self.vrep._id, "scenes/" + scene, 0, api.simx_opmode_oneshot_wait)
		if(code != api.simx_return_ok):
			raise VrepException("It failed to load scene! " + str(code))
		self.robot_base = self.vrep.joint.passive("hexa_base")
		self.joints = [self.vrep.joint.with_position_control("hexa_joint" + str(id+1)) for id in range(self.num_modules)]

	def chromoToJoints(self, chromo):
		assert(len(chromo) % 3 == 0)
		n = int(len(chromo) / 3)
		modules = []
		for i in range(n):
			amplitude = chromo[3 * i]
			ang_freq = chromo[3 * i + 1]
			phase = chromo[3 * i + 2]
			modules.append(Module(i, amplitude, ang_freq, phase))
		return modules

	def update_joint(self, id, pos):
		j = self.joints[id]
		api.simxSetJointTargetPosition(j._any_joint._id, j._any_joint._handle, pos, api.simx_opmode_oneshot)

	def close(self):
		code = api.simxStopSimulation(self.vrep.simulation._id, api.simx_opmode_oneshot_wait)
		if(code != api.simx_return_novalue_flag and code != api.simx_return_ok):
			raise VrepException("It failed to stop simulation! " + str(code))
		code, r = api.simxGetInMessageInfo(self.vrep.simulation._id, api.simx_headeroffset_server_state)
		while(r & 0b1):
			code, r = api.simxGetInMessageInfo(self.vrep.simulation._id, api.simx_headeroffset_server_state)

	def step(self):
		resume = True
		if self.callback is not None:
			resume = self.callback(self.t)
		api.simxSynchronousTrigger(self.vrep._id)
		self.t = self.t + self.timestep
		return self.t if resume else None

	def set_chromo(self, chromo):
		self.modules = self.chromoToJoints(chromo)
		#print("Created ", len(self.modules), " modules")
		#print_modules(self.modules)

	def get_pos(self):
		code, pos = api.simxGetObjectPosition(self.robot_base._any_joint._id, self.robot_base._any_joint._handle, -1, self.robot_base._any_joint._def_op_mode)
		if(code != api.simx_return_ok):
			raise VrepException("failed to get position! " + str(code))
		return np.array(pos[:2]) # Get the x,y coordinates
		
	def get_collision(self):
		code, val = api.simxGetIntegerSignal(self.vrep._id, "collision_signal", api.simx_opmode_streaming)
		if(code != api.simx_return_ok and code != api.simx_return_novalue_flag and not api.simx_return_remote_error_flag):
			raise VrepException("failed to get collision signal " + str(code))
		return val == 1

	def init(self):
		self.t = 0
		tries = 5
		while(tries > 0):
			code = api.simxStartSimulation(self.vrep.simulation._id, api.simx_opmode_oneshot_wait)
			if(code != api.simx_return_ok):
				tries = tries - 1
			else:
				return
		raise VrepException("Timeout starting!")
		
	def start(self, timeout=None, callback=None):
		self.callback = callback
		super().start(timeout)

class SimulationManager():
	def __init__(self, num_controllers, start_port, timestep, num_modules, scene, singularity=None):
		m = Manager()
		self.port_queue = m.Queue(num_controllers)
		self.ps = []
		self.singularity = singularity
		for port in range(start_port, start_port+num_controllers):
			self.start_vrep(port)
			self.port_queue.put(port)
		time.sleep(10)
		print("Waiting succesful")
		self.timestep = timestep
		self.num_modules = num_modules
		self.scene = scene
		self.controllers = m.dict()
		atexit.register(self.cleanup)

	def create_controller(self):
		port = self.port_queue.get(False)
		return PhantomVrepController(self.timestep, port, self.num_modules, self.scene)

	def start_vrep(self, port):
		exe = os.environ["VREP"] + "coppeliaSim"
		if(self.singularity is None):
			print("Starting headless on", port)
			p = subprocess.Popen([exe, "-h", "-gREMOTEAPISERVERSERVICE_%d_FALSE_TRUE" % port], stdout=subprocess.PIPE).pid
		else:
			print("Starting headless on", port, "in singularity")
			p = subprocess.Popen(["stdbuf", "-o0", "-e0", "-i0", "singularity", "exec", self.singularity, exe, "-h", "-gREMOTEAPISERVERSERVICE_%d_FALSE_TRUE" % port], stdout=subprocess.PIPE).pid
		self.ps.append(p)
		print("Successfully started headless on", port)

	def get(self):
		id = current_process().pid
		if id not in self.controllers:
			self.controllers[id] = self.create_controller()
		return self.controllers[id]

	def cleanup(self):
		print("Running cleanup...")
		for p in self.ps:
			print("Killing process: {}".format(p))
			os.kill(p, signal.SIGTERM)
			



class VrepException(Exception):
  pass

def handleUncaughtException(exctype, value, trace):
	print("Exception hook called")
	oldHook(exctype, value, trace)
	if isinstance(value, VrepException):
		sys.exit(5)

print("Setting new exception hook")
sys.excepthook, oldHook = handleUncaughtException, sys.excepthook