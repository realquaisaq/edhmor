from evo_tools import distance
from analysis_tools import split_walk_results, clean_walk, create_walk_graph, fix_walk
import os
import pickle
import matplotlib.pyplot as plt
import sys
import glob

bad_for_fixer = ["base_newton_inc_16334"]

for glob_file in sys.argv[1:]:
	for path in glob.glob(glob_file):
		filename = os.path.basename(path)
		print(path)
		with open(path, "rb") as f:
			all_results = pickle.load(f)
		sim_results, rl_results = split_walk_results(all_results)
		
		experiment_name = filename.split(".")[0]
		origin_sim = experiment_name.split("_")[1]
		pos_and_label = []
		if(origin_sim != "all"):
			origin = [walk for sim,walk in sim_results if sim == origin_sim][0][-1]
		for sim,walk in sim_results:
			end_point = walk[-1]
			if(origin_sim != "all"):
				transf = distance(origin, end_point)
			else:
				transf = 0
			fit = distance((0,0), end_point)
			label = "%s (%.3f, %.3f)" % (sim, fit, transf)
			label = sim.upper() if sim == "ode" else sim.capitalize()
			pos_and_label.append((walk, label))

		for label, walk in rl_results:
			fix = experiment_name not in bad_for_fixer
			if fix:
				fix_walk(walk)
			end_point = walk[-1]
			fit = distance((0,0), end_point)
			#transf = distance(origin, end_point)
			#label = "RL %s (%.3f, %.3f)" % (label, fit, transf)
			label = ("RL %s" % label) + ("*" if fix else "")
			pos_and_label.append((walk, label))

		create_walk_graph(*pos_and_label)
		plt.savefig("graphs2/" + experiment_name + "_v2.png")
		#plt.show()
		plt.clf()
		plt.cla()
		plt.close()