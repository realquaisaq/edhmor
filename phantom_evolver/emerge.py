from math import sin, pi
import time

class Module:
	def __init__(self, id, amplitude_control, ang_freq_control, phase_control):
		self.id = id
		self.max_amplitude = 0.5 * pi # taken from EmergeModuleSet.java
		self.max_ang_freq = 2.0 # taken from EmergeModuleSet.java
		self.amplitude_control = amplitude_control
		self.ang_freq_control = ang_freq_control
		self.phase_control = phase_control
	
	def get_pos(self, time):
		# Replicating how it is done in modules.control.SinusoidalController#updateJoints
		amplitude = self.max_amplitude * self.amplitude_control
		ang_freq = self.max_ang_freq * self.ang_freq_control
		# BIG NOTE: In the original java code, there is a bug where phaseControl is int, so jvm rounds the division to 0 (if <180) or 1
		#rad = amplitude * sin(ang_freq * time + (1 if self.phase_control >= 180 else 0) * pi)
		rad = amplitude * sin(ang_freq * time + (self.phase_control / 180) * pi)
		# To degrees
		#return rad * 180 / pi
		return rad
		
def chromo_to_modules(chromo):
	assert (len(chromo) + 3) % 9 == 0
	n = int((len(chromo) + 3) / 9)
	modules = []
	for i in range(n):
		if chromo[i] == 1.0:
			amplitude = chromo[n * 4 - 3 + i]
			ang_freq = chromo[n * 5 - 3 + i]
			phase = chromo[n * 6 - 3 + i]
			modules.append(Module(i, amplitude, ang_freq, phase))
	return modules
	
def print_modules(modules):
	headers = ["id", "amp", "ang_freq", "phase"]
	row_format_headers = "{:<5} " + ("{:<10} " * (len(headers) - 1))
	row_format = "{:<5} " + ("{:<10.4f} " * (len(headers) - 1))
	print(row_format_headers.format(*headers))
	for module in modules:
		print(row_format.format(module.id, module.amplitude_control, module.ang_freq_control, module.phase_control))

class Controller:
	def __init__(self, chromo):
		self.set_chromo(chromo)
		print("Created ", len(self.modules), " modules")
		print_modules(self.modules)

	def __enter__(self):
		return self

	def __exit__(self, exc_type, exc_val, exc_tb):
		self.close()

	def update_joint(self, id, pos):
		pass

	def close(self):
		pass

	def step(self):
		pass

	def init(self):
		pass

	def set_chromo(self, chromo):
		self.modules = chromo_to_modules(chromo)

	def update_joints(self, time):
		new_pos = []
		for module in self.modules:
			self.update_joint(module.id, module.get_pos(time))

	def prep(self):
		self.update_joints(0)

	def start(self, timeout=None):
		self.init()
		while(True):
			t = self.step()
			if t is None or (timeout is not None and t > timeout):
				break
			else:
				self.update_joints(t)

	def prompt_lifecycle(self, timeout=None, skip=None):
		input("Press to prepare module start positions...")
		self.prep()
		input("Press to start walk...")
		self.start(timeout=timeout)

class RealtimeController(Controller):
	def __init__(self, chromo):
		super().__init__(chromo)

	def step(self):
		return time.time() - self.start_time

	def start(self, timeout=None):
		self.start_time = time.time()
		super().start(timeout)
