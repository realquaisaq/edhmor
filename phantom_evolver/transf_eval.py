from sim_manager import PhantomVrepController
import sys
from evo_tools import evaluator, distance
from analysis_tools import get_chromo, create_walk_graph, get_walk_from_rl
from deap import tools, creator, base
import pickle
import matplotlib.pyplot as plt
from os.path import basename
from concurrent.futures import ThreadPoolExecutor
from sim_manager import SimulationManager
import multiprocessing
from functools import partial

chromo = get_chromo(sys.argv[1])
origin_sim = sys.argv[1].split(".")[0].split("_")[1]

def eval_record(c_manager, scene_runs):
	scene, runs = scene_runs
	controller = c_manager.get()
	controller.load_scene(scene)
	walks = []
	for _ in range(runs):
		result = evaluator(chromo, lambda : controller, 15)
		walk = result[1]["walk"]
		sim = scene.split("/")[1].split("_")[0]
		walks.append((sim,walk))
	return walks

if __name__ == "__main__":
	c_manager = SimulationManager(3, 45576, 0.05, 18, "phantom/bullet_50ms.ttt", None)

	with multiprocessing.Pool(3) as executor:
		target = partial(eval_record, c_manager)
		sim_results = executor.map(target, [("phantom/bullet_50ms.ttt", 1), ("phantom/ode_50ms.ttt", 3), ("phantom/newton_50ms.ttt", 1)])
		sim_results = [item for sublist in sim_results for item in sublist]
	
	if(len(sys.argv) >= 3):
		files = sys.argv[2:]
		for rl_data_file,i in zip(files, range(len(files))):
			label = rl_data_file.split(".")[0].split("_")[-1]
			walk = get_walk_from_rl(rl_data_file)
			sim_results.append(("rl_" + label, walk))

	with open("walks/" + basename(sys.argv[1]), "wb+") as f:
		pickle.dump(sim_results, f)