import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import sys

if(len(sys.argv) != 3):
	print("Requires at least one argument that is the output file")

file = sys.argv[1]
x, y = [], []
gen = -1
with open(file, "r") as f:
	for line in f:
		if line.startswith("Starting gen"):
			x.append(gen)
			y.append(0)
			gen += 1
		if gen >= 0 and line.startswith("Collision detected"):
			y[gen] = y[gen] + 1

y = [(c*100/50)/2 for c in y]

fig, ax = plt.subplots()
ax.plot(x, y)
ax.set_ylabel('Percentage of individuals skipped')
ax.set_xlabel('Generations')
ax.set_title(sys.argv[2])
ax.yaxis.set_major_formatter(mtick.PercentFormatter())
plt.show()