import pickle
import sys
import os
from evo_tools import distance
from statistics import mean
from math import atan2, pi
from analysis_tools import split_walk_results, fix_walk
from similarity_score import *
from scipy.stats import mannwhitneyu

base = []
main = []
for filename in os.listdir("walks"):
	path = "walks/" + filename
	experiment_name = filename.split(".")[0]
	phase = experiment_name.split("_")[0]
	with open(path, "rb") as f:
		if(phase == "main"):
			main.append((experiment_name, pickle.load(f)))
		elif(phase == "base"):
			base.append((experiment_name, pickle.load(f)))
		elif(phase == "transf" or experiment_name in exclude):
			continue
		else:
			raise Exception("Unknown experiment " + phase)

def error(all_results, err_f):
	sim_results, rl_results = split_walk_results(all_results)
	err = 0
	for _,rl_walk in rl_results:
		fix_walk(rl_walk)
		for _,sim_walk in sim_results:
			
			err += err_f(rl_walk, sim_walk)
	return err

print("\nFitness")
u,p = mannwhitneyu([error(a, fitness_diff) for (name, a) in base], [error(a, fitness_diff) for (name, a) in main])
print("U","p", sep="\t")
print(u,p, sep="\t")

print("\nOrientation")
u,p = mannwhitneyu([error(a, orientation_diff) for (name, a) in base], [error(a, orientation_diff) for (name, a) in main])
print("U","p", sep="\t")
print(u,p, sep="\t")

print("\nShape")
u,p = mannwhitneyu([error(a, shape_diff) for (name, a) in base], [error(a, shape_diff) for (name, a) in main])
print("U","p", sep="\t")
print(u,p, sep="\t")

print("\nTransferability")
u,p = mannwhitneyu([error(a, disparity) for (name, a) in base], [error(a, disparity) for (name, a) in main])
print("U","p", sep="\t")
print(u,p, sep="\t")