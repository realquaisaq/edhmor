from sim_manager import PhantomVrepController
import sys
from evo_tools import evaluator_transferability, WeightedSumFitness
from os import path
import pickle
from deap import tools, creator, base

if(len(sys.argv) != 6):
	print("Requires two argument that is the chromosome, timeout, transfer boundary, scene1, and scene2")

if(path.exists(sys.argv[1])):
	creator.create("FitnessMax", base.Fitness, weights=(0,)) # dummy fitness
	creator.create("Metrics", dict)
	creator.create("Individual", list, fitness=creator.FitnessMax, metrics=creator.Metrics)
	with open(sys.argv[1], "rb") as f:
		hof = pickle.load(f)
	chromo = hof[0]
else:
	chromo_str = sys.argv[1]
	chromo_str = chromo_str[1:] # Get rid of starting '['
	chromo_str = chromo_str[:-1] # Get rid of trailing ']'
	chromo = [float(i) for i in chromo_str.split(", ")]

if(len(sys.argv) == 7):
	hof = tools.HallOfFame(maxsize=1)
	creator.create("FitnessMax", WeightedSumFitness, weights=(1, 0))
	creator.create("Metrics", dict)
	creator.create("Individual", list, fitness=creator.FitnessMax, metrics=creator.Metrics)

timeout = float(sys.argv[2])
transfer_boundary = float(sys.argv[3])
scene1 = sys.argv[4]
scene2 = sys.argv[5]

controller = PhantomVrepController(0.05, 19997, 18, "phantom/newton_50ms.ttt")
result = evaluator_transferability(chromo, lambda : controller, timeout, transfer_boundary, scene1, scene2, allow_collisions=True)
if(len(sys.argv) == 7):
	(fit, met) = result
	print(fit)
	print(met)
	ind = creator.Individual()
	for v in chromo:
		ind.append(v)
	ind.fitness.values = fit
	ind.metrics.update(met)
	hof.update([ind])
	with open(sys.argv[6], 'wb+') as f:
		pickle.dump(hof, f)

print("Result:", result)