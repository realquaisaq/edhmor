import pickle
from deap import creator, base
from os import path
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
from matplotlib.lines import Line2D
from math import cos, sin, pi, sqrt, acos, degrees, atan2
import numpy as np
import codecs
from evo_tools import distance

def get_chromo(v):
	if(path.exists(v)):
		creator.create("FitnessMax", base.Fitness, weights=(0,)) # dummy fitness
		creator.create("Metrics", dict)
		creator.create("Individual", list, fitness=creator.FitnessMax, metrics=creator.Metrics)
		with open(v, "rb") as f:
			hof = pickle.load(f)
		return hof[0]
	else:
		chromo_str = v
		chromo_str = chromo_str[1:] # Get rid of starting '['
		chromo_str = chromo_str[:-1] # Get rid of trailing ']'
		return [float(i) for i in chromo_str.split(", ")]

def get_walk_from_rl(v):
	if(path.exists(v)):
		with open(v, 'r') as f:
			rl_data = f.read()
			#encoded_text = f.read()    #you should read in binary mode to get the BOM correctly
			#bom = codecs.BOM_UTF16_LE                                      #print dir(codecs) for other encodings
			#assert encoded_text.startswith(bom)                           #make sure the encoding is what you expect, otherwise you'll get wrong data
			#encoded_text = encoded_text[len(bom):]                         #strip away the BOM
			#rl_data = encoded_text.decode('utf-16le')
	else:
		rl_data = v
	walk = []
	for line in rl_data.splitlines(False):
		if line == "ERROR":
			walk.append(None)
		elif line != "":
			numbers = line.split(" ")
			walk.append((float(numbers[0]), float(numbers[1])))
	angle_a, angle_b = walk[0][0], walk[0][1]
	start_x, start_y = walk[1][0], walk[1][1]
	walk = walk[1:] # Get rid of start angle
	# Record None positions
	none_i = [i for i,pos in enumerate(walk) if pos is None]
	walk = [pos for pos in walk if pos is not None]
	
	# SteamVR is a right-hand coordinate system, so we negate y-coordinate.
	normalized_walk = [(x-start_x, -(y-start_y)) for x,y in walk]
	
	# Lets rotate all points!
	x = cos(angle_a * pi / 180.0)*cos(angle_b * pi / 180.0)
	y = sin(angle_b * pi / 180.0)
	if angle_b > 90 or angle_b < -90:
		x = -x
	angle = atan2(y, x)
	normalized_walk = [(cos(angle)*x-sin(angle)*y, sin(angle)*x+cos(angle)*y) for x,y in normalized_walk]
	
	# Return none positions
	none_i.sort()
	for i in none_i:
		walk.insert(i, None)
	
	return normalized_walk

def midpoint(x1,y1,x2,y2,i,k):
	return (x1+(i/k)*(x2-x1), y1+(i/k)*(y2-y1))

def fix_walk(walk):	
	if distance((0,0), walk[0]) > 0.1:
		walk[0] == (0,0)
	i = 1
	while(i < len(walk)-1):
		pos = walk[i]
		previous = walk[i-1]
		# Is this a bad point?
		#print(distance(pos, previous))
		if pos is None or distance(pos, previous) > (0.075 if len(walk) > 31 else 0.25):
			# Find the final point in this bad batch, where it jumps back to correct
			k = 2
			next = None
			#print("Found error at ", i, pos)
			for j in range(i+1, len(walk)-1):
				next = walk[j]
				if(distance(previous, next) < (k-1)*(0.05 if len(walk) > 31 else 0.1)):
					#print("Fixing to", j, next, distance(previous, next))
					break
				k += 1
			if next is None:
				#raise Exception("Failed to find next correct point")
				print("Failed to find next correct point")
				return walk
			for j in range(i, i+k-1):
				#print("Estimating",j)
				walk[j] = midpoint(previous[0], previous[1], next[0], next[1], j-i+1, k)
			i += k
		i += 1
	return walk

def clean_walk(walk):
	if distance((0,0), walk[0]) > 0.1:
		walk[0] == (0,0)
	for i,pos in enumerate(walk[1:-1], start=1):
		previous = walk[i-1]
		if pos is None or distance(pos, previous) > 0.2:
			del walk[i]

def split_walk_results(all_results):
	sim_results = [(sim, walk) for (sim, walk) in all_results if not sim.startswith("rl")]
	rl_results = [(sim.split("_")[1], walk) for (sim, walk) in all_results if sim.startswith("rl")]
	return (sim_results, rl_results)

def create_walk_graph(*kwargs):
	arrows = []
	labels = []
	cmap = plt.cm.jet
	cNorm  = colors.Normalize(vmin=0, vmax=len(kwargs))
	scalarMap = cmx.ScalarMappable(norm=cNorm,cmap=cmap)
	fig, ax = plt.subplots()
	i = 0
	x_min = float("inf")
	x_max = -float("inf")
	y_min = float("inf")
	y_max = -float("inf")
	for walk,label in kwargs:
		x_list, y_list = zip(*walk)
		colorVal = scalarMap.to_rgba(i)
		line = Line2D(x_list, y_list, label=label, color=colorVal)
		ax.add_line(line)
		#ax.plot(x_list, y_list, marker='o', markersize=1, color="green", ls='')
		arrows.append(line)
		labels.append(label)
		i = i + 1
		x_max = max(x_max, *x_list)
		x_min = min(x_min, *x_list)
		y_max = max(y_max, *y_list)
		y_min = min(y_min, *y_list)
	# Set view
	xy_max = max(abs(x_max), abs(x_min), abs(y_max), abs(y_min))
	#ax.set_xlim(x_min, x_max)
	#ax.set_ylim(y_min, y_max)
	#ax.set_xlim(-xy_max, xy_max)
	#ax.set_ylim(-xy_max, xy_max)
	margin = 0.1
	if xy_max == abs(x_max) or xy_max == abs(x_min):
		ax.set_xlim(right=x_max+margin, left=x_min-margin)
		length = abs(x_max - x_min)
		padding = (length - abs(y_max - y_min))/2
		ax.set_ylim(y_min-padding-margin, y_max+padding+margin)
	elif xy_max == abs(y_max) or xy_max == abs(y_min):
		ax.set_ylim(top=y_max+margin, bottom=y_min-margin)
		length = abs(y_max - y_min)
		padding = (length - abs(x_max - x_min))/2
		ax.set_xlim(x_min-padding-margin, x_max+padding+margin)
	ax.margins(1, tight=False)
	
	#ax.set_aspect(1/ax.get_data_ratio())
	ax.set_aspect('equal', adjustable='box')
	ax.grid(True, which='both')
	# set the x-spine (see below for more info on `set_position`)
	ax.spines['left'].set_position('zero')
	# turn off the right spine/ticks
	ax.spines['right'].set_color('none')
	ax.yaxis.tick_left()
	# set the y-spine
	ax.spines['bottom'].set_position('zero')
	# turn off the top spine/ticks
	ax.spines['top'].set_color('none')
	ax.xaxis.tick_bottom()
	
	
	# X-axis arrow
	ax.annotate('X [m]', xy=(0.99, -0.005), xycoords=('axes fraction', 'data'), 
				xytext=(5, 0), textcoords='offset points',
				ha='left', va='center')
				#arrowprops=dict(arrowstyle='<|-', fc='black'))

	# Y-axis arrow
	ax.annotate('Y [m]', xy=(0, 0.99), xycoords=('data', 'axes fraction'), 
				xytext=(0, 5), textcoords='offset points',
				ha='center', va='bottom')
				#arrowprops=dict(arrowstyle='<|-', fc='black'))
	
	plt.legend(arrows, labels, bbox_to_anchor=(1, 1), loc='upper left')
	plt.tight_layout(rect=[0,0,1,1])
	return (fig, ax)