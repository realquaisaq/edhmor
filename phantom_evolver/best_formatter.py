import sys
from deap.tools import HallOfFame
from deap import creator, base
import pyperclip
import pickle

creator.create("FitnessMax", base.Fitness, weights=(0,)) # dummy fitness
creator.create("Metrics", dict)
creator.create("Individual", list, fitness=creator.FitnessMax, metrics=creator.Metrics)

file = sys.argv[1]
format = sys.argv[2]

def format_sim(b):
	return str(b)

def format_rl(b):
	txt = str(b[0])
	for v in b[1:]:
		txt = txt + "\r\n" + str(v)
	return txt

with open(file, "rb") as f:
	hof = pickle.load(f)
	b = hof[0]
	if format == "rl":
		txt = format_rl(b)
	elif format == "sim":
		txt = format_sim(b)
	else:
		print("fuk u")
		sys.exit(1)
	print(txt)
	pyperclip.copy(txt)
	print("Copied that shit")