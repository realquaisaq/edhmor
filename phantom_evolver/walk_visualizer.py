from deap.tools import HallOfFame
from deap import creator, base
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import pickle
import sys
import numpy as np

if(len(sys.argv) < 2):
	print("Requires at least one argument that is the logbook file")


creator.create("FitnessMax", base.Fitness, weights=(0,)) # dummy fitness
creator.create("Metrics", dict)
creator.create("Individual", list, fitness=creator.FitnessMax, metrics=creator.Metrics)

arrows = []
labels = []

cmap = plt.cm.jet
cNorm  = colors.Normalize(vmin=0, vmax=len(sys.argv))
scalarMap = cmx.ScalarMappable(norm=cNorm,cmap=cmap)

fig, ax = plt.subplots()

for i in range(1, len(sys.argv)):
	file = sys.argv[i]
	with open(file, "rb") as cp_file:
		hof = pickle.load(cp_file)
	print(hof[0].metrics)
	print(hof[0].fitness.values)
	pos1 = hof[0].metrics["end_pos1"]
	pos2 = hof[0].metrics["end_pos2"]
	colorVal = scalarMap.to_rgba(i)
	arrows.append(ax.arrow(0, 0, pos1[0], pos1[1], label=file + " 1", color=colorVal))
	arrows.append(ax.arrow(0, 0, pos2[0], pos2[1], label=file + " 1", color=colorVal))
	labels.append(file + " 1")
	labels.append(file + " 2")

ax.set_aspect('equal')
ax.grid(True, which='both')

# set the x-spine (see below for more info on `set_position`)
ax.spines['left'].set_position('zero')

# turn off the right spine/ticks
ax.spines['right'].set_color('none')
ax.yaxis.tick_left()

# set the y-spine
ax.spines['bottom'].set_position('zero')

# turn off the top spine/ticks
ax.spines['top'].set_color('none')
ax.xaxis.tick_bottom()

plt.legend(arrows, labels, bbox_to_anchor=(1, 1), loc='upper left')
plt.tight_layout(rect=[0,0,1,1])
plt.show()