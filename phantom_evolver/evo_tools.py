import numpy as np
from deap import creator, base, tools, algorithms
from math import pi
import argparse
import pickle
from collections.abc import Sequence
import time
import random
import re
import os
from sim_manager import PhantomVrepController, SimulationManager
import multiprocessing
import random

def get_parser():
	parser = argparse.ArgumentParser()
	parser.add_argument("-H", "--headless",			dest="headless", action="store_true", help="Whether to use headless vrep mode")
	parser.add_argument("-P", "--port", 			dest="headless_port", type=int, default=45576, help="What port the headless vrep client should use")
	parser.add_argument("-t", "--timeout", 			dest="timeout", type=int, default=3, help="Fitness evaluation timeout")
	parser.add_argument("-p", "--population", 		dest="population", type=int, default=3, help="Evolution population size")
	parser.add_argument("-g", "--generations", 		dest="generations", type=int, default=3, help="Evolution generations")
	parser.add_argument("-m", "--motors", 			dest="motors", type=int, default=18, help="Amount of motors to use")
	parser.add_argument("-s", "--scene", 			dest="scene", help="Vrep scene to evaluate on", required=True)
	parser.add_argument("-T", "--timestep", 		dest="timestep", type=float, default=0.05, help="Vrep scene timestep")
	parser.add_argument("-c", "--concurrent", 		dest="pool_size", type=int, default=1, help="Amount of concurrent vrep processes")
	parser.add_argument("-S", "--singularity", 		dest="singularity", default=None, help="Specify singularity container path")
	parser.add_argument("-l", "--log-output", 		dest="log_output", default=None, help="Output file for logging")
	parser.add_argument("-b", "--best-output", 		dest="best_output", default=None, help="Output file for hall of fame")
	parser.add_argument("-C", "--checkpoint", 		dest="checkpoint", default=None, help="Checkpoint file to start from")
	parser.add_argument("-f", "--checkpoint-folder", dest="checkpoint_folder", default="checkpoints", help="Folder to place checkpoints")
	parser.add_argument("-F", "--checkpoint-freq", 	dest="checkpoint_freq", type=int, default=20, help="How often to save a checkpoint")
	parser.add_argument("-r", "--resume", 			dest="resume", action="store_true", help="Automatically find the last checkpoint and start from there")
	parser.add_argument("--indpb", 					dest="indpb", type=float, default=0.4, help="Probability of each value to be mutated")
	parser.add_argument("--cxpb", 					dest="cxpb", type=float, default=0.5, help="Probability of crossover")
	parser.add_argument("--mutpb", 					dest="mutpb", type=float, default=0.1, help="Probability of mutation")
	parser.add_argument("-L", "--limit",			dest="limit", type=int, default=None, help="Only do execute this many generations")
	parser.add_argument("--no-collisions",			dest="no_collisions", action="store_true", help="Whether to allow for leg collisions")
	parser.add_argument("--init-no-collisions",		dest="init_nc", action="store_true", help="Initial population has no collision")
	parser.add_argument("--mu",		dest="mu", type=float, default=0, help="Mu of gaussian mutation")
	parser.add_argument("--sigma1", dest="sigma1", type=float, default=1, help="Sigma of gaussian mutation for ang ang freq")
	parser.add_argument("--sigma2", dest="sigma2", type=float, default=1, help="Sigma of gaussian mutation for phase")
	return parser

def get_toolbox(args, c_getter):
	toolbox = base.Toolbox()
	toolbox.register("attr_float", random.uniform, 0, 1)
	toolbox.register("attr_int", random.randint, 0, 360)
	toolbox.register("individual", tools.initCycle, creator.Individual, (toolbox.attr_float, toolbox.attr_float, toolbox.attr_int), n=args.motors)
	toolbox.register("mate", tools.cxTwoPoint)
	toolbox.register("mutate", mutGaussianBounded, mu=args.mu, sigma=[args.sigma1,args.sigma1,args.sigma2]*args.motors, low=0, up=[1,1,360]*args.motors, indpb=args.indpb)
	toolbox.register("select", tools.selTournament, tournsize=3)
	
	if(args.init_nc):
		toolbox.register("population", init_nc_population, list, toolbox.individual, c_getter=c_getter, toolbox=toolbox)
	else:
		toolbox.register("population", tools.initRepeat, list, toolbox.individual)
	
	if(args.headless):
		pool = multiprocessing.Pool(args.pool_size)
		toolbox.register("map", pool.map)
	
	return toolbox

def get_controller_getter(args):
	print("Running headless:", args.headless)
	print("Running with", args.pool_size, "processes")
	if(args.headless):
		# The plus on is because we also want one for getting start position here in main thread
		return SimulationManager(args.pool_size, args.headless_port, args.timestep, args.motors, args.scene, args.singularity).get
	else:
		c = PhantomVrepController(args.timestep, 19997, args.motors, args.scene)
		return lambda : c
		
def get_objects(args, toolbox):
	# Create the checkpoint folder if it does not exist
	if(args.checkpoint_folder is not None):
		from pathlib import Path
		Path(args.checkpoint_folder).mkdir(parents=True, exist_ok=True)

	# Find latest checkpoint if resume flag set
	if(args.resume):
		file_names = [f for f in os.listdir(args.checkpoint_folder) if os.path.isfile(os.path.join(args.checkpoint_folder, f))]
		if(len(file_names) > 0):
			regex = re.compile(r"checkpoint_([0-9]+)\.pkl")
			files = []
			for f in file_names:
				match = regex.match(f)
				if(match):
					gen = int(match.groups()[0])
					files.append((gen, f))
			if(len(files) > 0):
				checkpoint = max(files, key=lambda x: x[0])[1]
				args.checkpoint = args.checkpoint_folder + "/" + checkpoint

	# Load checkpoint or start normal
	if(args.checkpoint is not None):
		print("Loading checkpoint:", args.checkpoint)
		with open(args.checkpoint, "rb") as cp_file:
			cp = pickle.load(cp_file)
		population = cp["population"]
		start_gen = cp["generation"]
		hof = cp["halloffame"]
		logbook = cp["logbook"]
		random.setstate(cp["rndstate"])
	else:
		print("Starting evolution from scratch")
		population = toolbox.population(n=args.population)
		start_gen = 0
		hof = tools.HallOfFame(maxsize=1)
		logbook = tools.Logbook()

	# Set max generation
	if(args.limit is not None):
		generations = min(start_gen + args.limit, args.generations)
	else:
		generations = args.generations
	print("Running", generations-start_gen, "generations")
	
	return (generations, population, start_gen, hof, logbook)
	
def save_objects(args, hof, logbook):
	if(args.log_output is not None):
		with open(args.log_output, 'wb+') as f:
			pickle.dump(logbook, f)
	if(args.best_output is not None):
		with open(args.best_output, 'wb+') as f:
			pickle.dump(hof, f)

def distance(start_pos, end_pos):
	start_pos = np.array(start_pos)
	end_pos = np.array(end_pos)
	return np.linalg.norm(end_pos - start_pos)

def orientation(start_pos, end_pos):
	end_pos = end_pos - start_pos
	end_pos = end_pos / np.linalg.norm(end_pos)
	unit_vector = [1,0]
	rad = np.arccos(np.clip(np.dot(unit_vector, end_pos), -1.0, 1.0))
	return (rad * 180) / pi
	
def get_tracker_callback(l, controller, recording_frequency,collision):
	def tracker_callback(t):
		if np.isclose(t % recording_frequency, 0, 0.00001) or np.isclose(t % recording_frequency, recording_frequency, 0.00001):
			l.append(controller.get_pos())
		if(collision is not None and controller.get_collision()):
			collision[0] = True
			print("Collision detected, stopping early")
			return False
		return True
			
	return tracker_callback

def evaluator(individual, getter, timeout, allow_collisions=True):
	controller = getter()
	start_pos = controller.get_pos()
	# Run robot
	controller.set_chromo(individual)
	walk_list = []
	collision = [False]
	controller.start(timeout = timeout, callback=get_tracker_callback(walk_list, controller, 0.5, collision=collision if not allow_collisions else None))
	# Calc fitness
	end_pos = controller.get_pos()
	controller.close()
	metrics = {}
	metrics["end_pos"] = end_pos
	metrics["walk"] = walk_list
	metrics["collided"] = collision[0]
	if(collision[0]):
		return (0,),metrics
	return (distance(start_pos, end_pos),),metrics

def evaluator_pos(individual, controller, timeout, scene, callback):
	controller.load_scene(scene)
	start_pos = controller.get_pos()
	controller.set_chromo(individual)
	collision = [False]
	controller.start(timeout = timeout, callback=callback)
	end_pos = controller.get_pos()
	controller.close()
	return (distance(start_pos, end_pos), end_pos)

def evaluator_transferability(individual, getter, timeout, transfer_boundary, scene1, scene2, allow_collisions=True):
	if(isinstance(scene1, list)):
		scene1 = random.choice(scene1)
	if(isinstance(scene2, list)):
		if scene1 in scene2:
			scene2 = list(scene2)
			scene2.remove(scene1)
		scene2 = random.choice(scene2)
	controller = getter()
	walk1 = []
	walk2 = []
	collision = [False]
	collision1 = collision if not allow_collisions else None
	dist1, end_pos1 = evaluator_pos(individual, controller, timeout, scene1, get_tracker_callback(walk1, controller, 0.5, collision=collision1))
	dist2, end_pos2 = evaluator_pos(individual, controller, timeout, scene2, get_tracker_callback(walk2, controller, 0.5, collision=collision1))
	transfer_difference = distance(end_pos1, end_pos2)
	metrics = {}
	metrics["end_pos1"] = end_pos1
	metrics["end_pos2"] = end_pos2
	metrics["walk1"] = walk1
	metrics["walk2"] = walk2
	metrics["collided"] = collision[0]
	if(transfer_difference > transfer_boundary or collision[0]):
		return (0, 0),metrics
	else:
		return (dist1, transfer_difference),metrics

def stretch(l, size):
	if not isinstance(l, Sequence):
		return [l] * size
	elif len(l) < size:
		raise IndexError("argument must be at least the size of individual: %d < %d" % (len(l), size))
	else:
		return l

def mutUniform(individual, low, up, indpb):
	size = len(individual)
	low = stretch(low, size)
	up = stretch(up, size)
	for i, xl, xu in zip(range(size), low, up):
		if random.random() < indpb:
			individual[i] = random.uniform(xl, xu)
	return individual,

def mutGaussianBounded(individual, low, up, mu, sigma, indpb):
	size = len(individual)
	low = stretch(low, size)
	up = stretch(up, size)
	mu = stretch(mu, size)
	sigma = stretch(sigma, size)
	for i, xl, xu, xm, xs in zip(range(size), low, up, mu, sigma):
		if random.random() < indpb:
			v = individual[i] + random.gauss(xm, xs)
			v = max(v, xl)
			v = min(v, xu)
			individual[i] = v
	return individual,

def save_checkpoint(checkpoint_folder, gen, population, halloffame, logbook):
	cp = dict(population=population, generation=gen, halloffame=halloffame, logbook=logbook, rndstate=random.getstate())
	with open("{}/checkpoint_{}.pkl".format(checkpoint_folder, gen), "wb+") as cp_file:
		pickle.dump(cp, cp_file)

def evolve(toolbox, population, generations, cxpb, mutpb, start_gen=0, logbook=None, stats=None, halloffame=None, checkpoint_freq=None, checkpoint_folder="."):
	if(logbook):
		logbook.header = ['gen', 'nevals'] + (stats.fields if stats else [])
	start_time = time.time()
	for gen in range(start_gen, generations):
		print("Starting gen", gen)
		# Mate and mutate
		old_population = population
		population = algorithms.varAnd(population, toolbox, cxpb=cxpb, mutpb=mutpb)
		# Evaluate the individuals with an invalid fitness
		invalid_ind = [ind for ind in population if not ind.fitness.valid]
		try:
			metrics = toolbox.map(toolbox.evaluate, invalid_ind)
		except Exception as e:
			if checkpoint_freq is not None:
				save_checkpoint(checkpoint_folder, gen, old_population, halloffame, logbook)
			raise e
		for ind, (fit, met) in zip(invalid_ind, metrics):
			ind.fitness.values = fit
			if met is not None:
				ind.metrics.update(met)
		# Record
		if(halloffame is not None):
			halloffame.update(population)
		if(logbook is not None):
			record = stats.compile(population) if stats else {}
			logbook.record(gen=gen, evals=len(invalid_ind), **record)
		# Select new population
		population = toolbox.select(population, k=len(population))
		if checkpoint_freq is not None and gen != start_gen and gen % checkpoint_freq == 0:
			save_checkpoint(checkpoint_folder, gen, population, halloffame, logbook)
	print("Evolution took", time.time() - start_time, "seconds")

def get_valid_individual(fc):
	func, c_getter = fc
	ind = func()
	_, metrics = evaluator(ind, c_getter, 5, allow_collisions=False)
	return get_valid_individual((func, c_getter)) if metrics["collided"] else ind

def init_nc_population(container, func, n, c_getter, toolbox):
	return container(toolbox.map(get_valid_individual, [(func, c_getter)] * n))


# Stolen from https://github.com/BlueBrain/BluePyOpt/blob/master/bluepyopt/deapext/optimisations.py
class WeightedSumFitness(base.Fitness):
	def __init__(self, values=()):
		super(WeightedSumFitness, self).__init__(values)

	@property
	def weighted_sum(self):
		return sum(self.wvalues)

	@property
	def sum(self):
		return sum(self.values)

	def __le__(self, other):
		return self.weighted_sum <= other.weighted_sum

	def __lt__(self, other):
		return self.weighted_sum < other.weighted_sum

	def __deepcopy__(self, _):
		cls = self.__class__
		result = cls.__new__(cls)
		result.__dict__.update(self.__dict__)
		return result