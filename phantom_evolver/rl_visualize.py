from analysis_tools import create_walk_graph, get_walk_from_rl
import matplotlib.pyplot as plt
import sys

pos_and_label = []
files = sys.argv[1:]
for rl_data_file,i in zip(files, range(len(files))):
	walk = get_walk_from_rl(rl_data_file)
	pos_and_label.append((walk, rl_data_file))

create_walk_graph(*pos_and_label)
plt.show()
