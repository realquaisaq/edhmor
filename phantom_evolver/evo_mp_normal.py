from deap import creator, base, tools
import numpy as np
import evo_tools

parser = evo_tools.get_parser()
args = parser.parse_args()

# Start DEAP configuration
creator.create("FitnessMax", base.Fitness, weights=(1.0,)) # Distance and orientation, but we dont count orientation into descendants
creator.create("Metrics", dict)
creator.create("Individual", list, fitness=creator.FitnessMax, metrics=creator.Metrics)

if __name__ == '__main__':
	c_getter = evo_tools.get_controller_getter(args)
	toolbox = evo_tools.get_toolbox(args, c_getter)
	print("Running", "with" if(not args.no_collisions) else "without", "collisions")
	toolbox.register("evaluate", evo_tools.evaluator, getter=c_getter, timeout=args.timeout, allow_collisions=not args.no_collisions)

	generations, population, start_gen, hof, logbook = evo_tools.get_objects(args, toolbox)

	stats = tools.Statistics(key=lambda ind: ind.fitness.values[0])
	stats.register("avg", np.mean)
	stats.register("std", np.std)
	stats.register("min", np.min)
	stats.register("max", np.max)
	evo_tools.evolve(
		toolbox, population, generations, 
		start_gen=start_gen, halloffame=hof, 
		stats=stats, logbook=logbook, 
		checkpoint_freq=args.checkpoint_freq, 
		checkpoint_folder=args.checkpoint_folder,
		cxpb=args.cxpb, mutpb=args.mutpb
	)
	print("Best:", hof)
	evo_tools.save_objects(args, hof, logbook)
