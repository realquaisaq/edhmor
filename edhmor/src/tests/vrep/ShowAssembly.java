package tests.vrep;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import coppelia.IntW;
import coppelia.remoteApi;
import es.udc.gii.common.eaf.util.EAFRandom;
import modules.ModuleSetFactory;
import modules.evaluation.VrepCreateRobot;
import modules.evaluation.VrepSimulator;
import modules.individual.TreeIndividual;
import modules.util.ChromoConversion;
import modules.util.SimulationConfiguration;


public class ShowAssembly {

	public static void main(String[] args) {
		
        double[] chromosomeDouble = ChromoConversion.str2double("[(0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 15.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 , 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 15.0, 13.0, 14.0, 15.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.5455963591611365, 0.11371233636543576, 0.036290954196614544, 0.3903656017030154, 0.00679176572264828, 0.08146566559447732, 0.07535369694622207, 0.7730905441161025, 0.2500507281219915, 0.40313639025411774, 0.003973608347822122, 0.7781750911928096, 0.3899374167547881, 0.9600134640670561, 0.7250404836902793, 0.8445273695804674, 0.6467894800807368, 0.16030850687424925, 0.2018541954218076, 0.49567921698965856, 0.9019485291134028, 0.20829306079896515, 0.06612457688359719, 0.49610490207718816, 0.7488436794002526, 0.9645264481423355, 0.5163520317251132, 0.2457172378705993, 0.41377103759169165, 0.12842989434914487, 0.35238585296706193, 0.22133798867604992, 344.4679226211596, 309.281008589332, 266.8072439685561, 134.9338321211479, 12.159645687248428, 57.23409787318109, 232.8997112761175, 90.64154969188141, 121.74772800789312, 252.27177708222214, 66.17009812495084, 11.886793305793866, 11.792446546844415, 205.59952213863193, 79.29145620359417, 30.7604219708471, 0.4378857762257965, -0.4539603364820609, -0.018494123142167918, 0.49687190619165233, -0.43881005244237103, 0.39648466172584684, -0.304664078161781, -0.4413269361659312, 0.2201318672469349, -0.39377771322649235, -0.3754566640525431, -0.3969155009689903, 0.017185949030072756, -0.21401444995093033, 0.1949855550960401, -0.3794066650961495, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0] - 1.7976931348623157E308");
        //[(0.0, 1.0, 1.0, 1.0, 1.0, 2.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.9647947711270195, 0.008009881533388108, 0.6883638417963029, 0.7630390764666481, 0.15987048338258558, 0.6844859409006773, 0.5762244731092748, 0.9133795675485866, 0.9692656950908475, 0.07847879564088978, 243.21618474989134, 312.03617356398917, 227.69359428880082, 68.58577136047361, 51.43940382439154, 0.3127737704574012, -0.1196665952271635, 0.0673736828957997, -0.030505011376149804, 0.24770325859837383, 0.0, 0.0, 0.0, 0.0, 0.0] - 0.9426395801130477
        String world = "baseEstandar";
        if (world.contains("manipulator")) {
                SimulationConfiguration.setManipulatorBase(true);
        } else {
                SimulationConfiguration.setManipulatorBase(false);
        }

        EAFRandom.init();
        TreeIndividual tree = new TreeIndividual();
        tree.init(141);
        tree.generate();
        
        //Set the correct module set to employ
        String moduleSet = "EmergeFlatBase";
        SimulationConfiguration.setModuleSet(moduleSet);
        ModuleSetFactory.reloadModuleSet();
        
        List<String> worldBase = new ArrayList<String>();
        worldBase.add("baseEstandar" + ".world");
        SimulationConfiguration.setWorldsBase(worldBase);

        VrepSimulator vrepSimulator =  new VrepSimulator(); 
        System.out.println("connecting to vrep...");
        vrepSimulator.connect2Vrep();
        
        remoteApi vrepApi = vrepSimulator.getVrepApi();
        int clientID = vrepSimulator.getClientID();
        
        VrepCreateRobot robot = new VrepCreateRobot(vrepApi, clientID, tree.clone().getChromosomeAt(0), "", true);
        robot.createRobot("scenes/edhmor/default.ttt");
        
        IntW pingTime = new IntW(0);
        vrepApi.simxGetPingTime(clientID, pingTime);
        
        vrepSimulator.disconnect();
	}
	


}
