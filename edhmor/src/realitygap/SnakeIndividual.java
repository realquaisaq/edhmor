package realitygap;

import modules.individual.Connection;
import modules.individual.Node;
import modules.individual.TreeIndividual;
import org.apache.commons.math.util.ResizableDoubleArray;

public class SnakeIndividual extends TreeIndividual {
    @Override
    public void generate() {
        int size = 141;
        ResizableDoubleArray[] zeroChromo = new ResizableDoubleArray[1];
        zeroChromo[0] = new ResizableDoubleArray(size);
        for (int i = 0; i < size; i++) {
            zeroChromo[0].setElement(i, 0.0);
        }
        this.setChromosomes(zeroChromo);

        int nodes = 5;
        Node rootNode = new Node(0,null);
        Node prevNode = rootNode;
        // Create chain
        for(int i = 0; i < nodes-1; i++) {
            Node node = new Node(1, prevNode);
            Connection conn = new Connection(prevNode, node, 5, 0);
            prevNode.addChildren(node, conn);
            prevNode = node;
        }

        this.setRootNode(rootNode);
        this.modifyChromosome();
    }
}
