package realitygap.dto;

import javax.vecmath.Vector2d;
import javax.vecmath.Vector3d;
import java.util.List;
import java.util.TreeMap;

public class SimulationRun {
    private final VrepSceneConfig sceneConfig;
    private final TreeMap<Double, Vector3d> positions;
    private final TreeMap<Double, List<Vector2d>> errors;
    private final double fitness;
    private final int moduleCount;
    private final int rerunIndex;

    public SimulationRun(VrepSceneConfig sceneConfig, TreeMap<Double, Vector3d> positions, TreeMap<Double, List<Vector2d>> errors, double fitness, int moduleCount, int rerunIndex) {
        this.sceneConfig = sceneConfig;
        this.positions = positions;
        this.errors = errors;
        this.fitness = fitness;
        this.moduleCount = moduleCount;
        this.rerunIndex = rerunIndex;
    }

    public VrepSceneConfig getSceneConfig() {
        return sceneConfig;
    }

    public TreeMap<Double, Vector3d> getPositions() {
        return positions;
    }

    public TreeMap<Double, List<Vector2d>> getErrors() {
        return errors;
    }

    public double getFitness() {
        return fitness;
    }

    public int getModuleCount() {
        return moduleCount;
    }

    public int getRerunIndex() {
        return rerunIndex;
    }
}
