package realitygap.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

public class RobotEvaluation {
    private final String dataDirectoryNumber;
    private final String runDirectoryName;
    private final int chromosomeCounter;
    private final List<Future<SimulationRun>> futureSimulationRuns;

    public RobotEvaluation(String dataDirectoryNumber, String runDirectoryName, int chromosomeCounter,
                           List<Future<SimulationRun>> futureSimulationRuns) {
        this.dataDirectoryNumber = dataDirectoryNumber;
        this.runDirectoryName = runDirectoryName;
        this.chromosomeCounter = chromosomeCounter;
        this.futureSimulationRuns = futureSimulationRuns;
    }

    public String getDataDirectoryNumber() {
        return dataDirectoryNumber;
    }

    public String getRunDirectoryName() {
        return runDirectoryName;
    }

    public int getChromosomeCounter() {
        return chromosomeCounter;
    }

    public List<Future<SimulationRun>> getFutureSimulationRuns() {
        return futureSimulationRuns;
    }
}
