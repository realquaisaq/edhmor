package realitygap.dto;

public class VrepSceneConfig {
    private final String engineName;
    private final String scenePath;
    private final double friction;
    private final double timeStep;

    public VrepSceneConfig(String scenePath, String engineName, double friction, double timeStep) {
        this.scenePath = scenePath;
        this.engineName = engineName;
        this.friction = friction;
        this.timeStep = timeStep;
    }

    public String getEngineName() {
        return this.engineName;
    }

    public String getScenePath() {
        return this.scenePath;
    }

    public double getTimeStep() {
        return this.timeStep;
    }

    public double getFriction() {
        return this.friction;
    }
}
