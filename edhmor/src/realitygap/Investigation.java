package realitygap;

import es.udc.gii.common.eaf.util.EAFRandom;
import modules.ModuleSetFactory;
import modules.evaluation.VrepSimulator;
import modules.individual.Connection;
import modules.individual.Node;
import modules.individual.TreeIndividual;
import modules.jeaf.application.edhmor.RoboticObjetiveFunction;
import modules.util.ChromoConversion;
import modules.util.SimulationConfiguration;
import org.apache.commons.math.util.ResizableDoubleArray;

import java.util.List;
import java.util.Locale;

public class Investigation {
    public static void main(String[] args) throws Exception {
        /*for(int modNode = 1; modNode <= 3; modNode++) {
            for (float ampl = 0; ampl <= 1; ampl += 0.2) {
                for (float freq = 0; freq <= 1; freq += 0.2) {
                    for (int phase = 0; phase <= 360; phase += 45) {
                        TreeIndividual tree = createSnake(ampl, freq, phase, modNode);
                        double fitness = new WalkObjetiveFunction().evaluate(tree.getChromosomeAt(0));
                        System.out.printf("%d %f %f %d = %f\n", modNode, ampl, freq, phase, fitness);
                    }
                }
            }
        }*/
        //TreeIndividual tree = createSnake(0, 0, 0, 3);
        //new WalkObjetiveFunction().evaluate(tree.getChromosomeAt(0));

        VrepSimulator vrepSimulator = new VrepSimulator();
        SimulationConfiguration.setVrep(vrepSimulator);
        vrepSimulator.connect2Vrep();
        //vrepSimulator.start();

        /*for (int ampl = 5; ampl <= 8; ampl++) {
            for (int phase = 0; phase <= 360; phase += 20) {
                String chromo = String.format(Locale.US, "[(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.5, 0.0, %f, 1, 0.0, 1, 180.0, 0.0, %d, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0] - 1.7976931348623157E308", ampl/10., phase);
                TreeIndividual tree = ChainModuleTest.create(ChromoConversion.str2double(chromo));
                testIndividual(tree, ampl, phase);
            }
        }*/
        float ampl = 0.9f;
        int phase = 240;
        String chromo = String.format(Locale.US, "[(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.5, 0.0, %f, 1, 0.0, 1, 180.0, 0.0, %d, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0] - 1.7976931348623157E308", ampl, phase);
        TreeIndividual tree = ChainModuleTest.create(ChromoConversion.str2double(chromo));
        testIndividual(tree, ampl, phase);
        //vrepSimulator.stop();
    }

    public static void testIndividual(TreeIndividual tree, float ampl, int phase) {
        List<Double> fitnesses = new RoboticObjetiveFunction().evaluate(tree.getChromosomeAt(0));
        //System.out.printf(Locale.US, "%f\t%d\t%f\n", ampl, phase, fitness);
        System.out.println(fitnesses.get(0));
    }

    public static TreeIndividual createSnake(float ampl, float freq, int phase, int changedNode) {
        EAFRandom.init();

        //Set the correct module set to employ
        String moduleSet = "EmergeModules";
        SimulationConfiguration.setModuleSet(moduleSet);
        ModuleSetFactory.reloadModuleSet();

        int size = 24;
        ResizableDoubleArray[] zeroChromo = new ResizableDoubleArray[1];

        zeroChromo[0] = new ResizableDoubleArray(size);

        for (int i = 0; i < size; i++) {
            zeroChromo[0].setElement(i, 0.0);
        }

        TreeIndividual tree = new TreeIndividual();
        tree.setChromosomes(zeroChromo);
        //Node modNode = new Node(1, 0, ampl, freq, phase, 0., 0., null);
        Node node1 = changedNode == 1 ? new Node(1, 0, ampl, freq, phase, 0., 0., null) :
                new Node(1, 0, 0.5, 0.5, 180, 0., 0., null);
        Node node2 = changedNode == 2 ? new Node(1, 0, ampl, freq, phase, 0., 0., node1) :
                new Node(1, 0, 0.5, 0.5, 90, 0., 0., node1);
        Connection conn1 = new Connection(node1, node2, 1, 1);
        node1.addChildren(node2, conn1);
        Node node3 = changedNode == 3 ? new Node(1, 0, ampl, freq, phase, 0., 0., node2) :
                new Node(1, 0, 0, 0, 0, 0., 0., node2);
        Connection conn2 = new Connection(node2, node3, 1, 1);
        node2.addChildren(node3, conn2);

        tree.setRootNode(node1);
        tree.modifyChromosome();
        return tree;
    }
}
