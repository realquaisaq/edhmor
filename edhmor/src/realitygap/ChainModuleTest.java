package realitygap;

import es.udc.gii.common.eaf.util.EAFRandom;
import modules.ModuleSetFactory;
import modules.evaluation.fitness.DistanceTravelledFitnessFunction;
import modules.evaluation.fitness.FitnessFunction;
import modules.individual.Connection;
import modules.individual.Node;
import modules.individual.TreeIndividual;
import modules.util.ChromoConversion;
import modules.util.SimulationConfiguration;
import org.apache.commons.math.util.ResizableDoubleArray;
import realitygap.dto.VrepSceneConfig;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ChainModuleTest {
    public static void main(String[] args) throws Exception {
        double[] chromosome = ChromoConversion.str2double("[(0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 0.0, 3.0, 2.0, 1.0, 2.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.07772546871098007, 0.7122957190528781, 0.07704574484933213, 0.11705686489517853, 0.47341873745279495, 0.5587620665774669, 0.4930171622704189, 0.22687746159122746, 0.5445978680449591, 0.9009093193899659, 0.9692743594209607, 0.7626537736563888, 133.41909098422747, 90.0903548281806, 52.209791221533315, 326.6712303830726, 84.2268059520339, 286.0858557261503, -0.11921916375603248, 0.3565633655211152, -0.009716443180592393, -0.17310305264176273, -0.06390915914335105, -0.1558310328779342, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0] - 2.2665170899742653");
        VrepSceneConfig sceneConfig = new VrepSceneConfig("scenes/edhmor/emerge_ode_0.6fric_50ms.ttt","ode",0.6,0.050);

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
        System.out.println(dtf.format(LocalDateTime.now()) + ": starting");

        MultiVrepEvaluator evaluator = new MultiVrepEvaluator(chromosome, sceneConfig.getScenePath(), sceneConfig.getTimeStep());
        System.out.println(dtf.format(LocalDateTime.now()) + ": now");
        evaluator.evaluate();
        System.out.println(dtf.format(LocalDateTime.now()) + ": done");

        FitnessFunction fitnessFunction = evaluator.getFitnessFunction();
        DistanceTravelledFitnessFunction distanceTravelledFitnessFunction = (DistanceTravelledFitnessFunction) fitnessFunction; //assume it is distance travelled function or subclass
        var startPosition = distanceTravelledFitnessFunction.getInitialPosition();
        var endPosition = distanceTravelledFitnessFunction.getFinalPosition();
        var fitness = distanceTravelledFitnessFunction.getFitness();



        System.out.println("distance travelled: " + fitness);
    }

    public static TreeIndividual create(double[] chromo) {
        EAFRandom.init();

        //Set the correct module set to employ
        String moduleSet = "EmergeModules";
        SimulationConfiguration.setModuleSet(moduleSet);
        ModuleSetFactory.reloadModuleSet();

        TreeIndividual tree = new TreeIndividual();
        ResizableDoubleArray[] arr = new ResizableDoubleArray[1];
        arr[0] = new ResizableDoubleArray();
        for(int i = 0; i < chromo.length; i++) {
            arr[0].setElement(i, chromo[i]);
        }
        tree.setChromosomes(arr);
        return tree;
    }

    public static TreeIndividual createSnake(int nodes) {
        EAFRandom.init();

        //Set the correct module set to employ
        String moduleSet = "EmergeModules";
        SimulationConfiguration.setModuleSet(moduleSet);
        ModuleSetFactory.reloadModuleSet();

        int size = 141;
        ResizableDoubleArray[] zeroChromo = new ResizableDoubleArray[1];

        zeroChromo[0] = new ResizableDoubleArray(size);

        for (int i = 0; i < size; i++) {
            zeroChromo[0].setElement(i, 0.0);
        }

        TreeIndividual tree = new TreeIndividual();
        tree.setChromosomes(zeroChromo);
        Node rootNode = new Node(1,null);
        Node prevNode = rootNode;
        // Create chain
        for(int i = 0; i < nodes-1; i++) {
            Node node = new Node(1, prevNode);
            Connection conn = new Connection(prevNode, node, 5, 0);
            prevNode.addChildren(node, conn);
            prevNode = node;
        }

        tree.setRootNode(rootNode);
        tree.modifyChromosome();
        return tree;
    }
}
