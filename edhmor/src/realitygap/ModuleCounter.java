package realitygap;

import es.udc.gii.common.eaf.util.EAFRandom;
import modules.ModuleSetFactory;
import modules.RobotFeatures;
import modules.evaluation.CalculateModulePositions;
import modules.evaluation.dynamicFeatures.DynamicFeatures;
import modules.individual.TreeIndividual;
import modules.util.ChromoConversion;
import modules.util.SimulationConfiguration;
import org.apache.commons.math.util.DoubleArray;
import org.apache.commons.math.util.ResizableDoubleArray;
import realitygap.dto.RobotEvaluation;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class ModuleCounter {

    public static void main(String[] args) {
        final String resultsPath = args[0];
        final String dataDirPattern = args[1];
        final String runDirPattern = args[2];
        final String generationToEvaluate = args[3];

        class DataProcessingDTO {
            String dataDirectoryName;
            String runDirectoryName;
            Path runDirectoryPath;
        }
        List<DataProcessingDTO> runsToProcess = new ArrayList<>();

        try (DirectoryStream<Path> dataDirectoryStream = Files.newDirectoryStream(new File(resultsPath).toPath(), dataDirPattern)) {
            dataDirectoryStream.forEach(dataDirectoryPath -> {
                String dataDirectoryName = dataDirectoryPath.getFileName().toString();
                try (DirectoryStream<Path> runDirectoryStream = Files.newDirectoryStream(dataDirectoryPath, runDirPattern)) {
                    runDirectoryStream.forEach(runDirectoryPath -> {
                        String runDirectoryName = runDirectoryPath.getFileName().toString();

                        DataProcessingDTO dto = new DataProcessingDTO();
                        dto.dataDirectoryName = dataDirectoryName;
                        dto.runDirectoryName = runDirectoryName;
                        dto.runDirectoryPath = runDirectoryPath;
                        runsToProcess.add(dto);
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        var dict = new TreeMap<String, Integer>();
        ArrayList<RobotEvaluation> robotEvaluations = new ArrayList<>();
        for (DataProcessingDTO dto : runsToProcess) {
            List<double[]> chromosomes = getGenerationChromosomes(dto.runDirectoryPath, generationToEvaluate);
            for (var chromosome : chromosomes) {
                var individual = create(chromosome);
                var moduleCount = new CalculateModulePositions(individual.getChromosomeAt(0)).getnModules();
                var dictKey = dto.runDirectoryName + ":\t" + Integer.toString(moduleCount);
                dict.put(dictKey, dict.getOrDefault(dictKey, 0) + 1);
            }
        }

        for (var x : dict.keySet()) {
            if (!x.endsWith("6"))
                continue;
            System.out.println(x + ": " + dict.get(x));
        }
    }

    public static TreeIndividual create(double[] chromo) {
        EAFRandom.init();

        //Set the correct module set to employ
        String moduleSet = "EmergeModules";
        SimulationConfiguration.setModuleSet(moduleSet);
        ModuleSetFactory.reloadModuleSet();

        TreeIndividual tree = new TreeIndividual();
        ResizableDoubleArray[] arr = new ResizableDoubleArray[1];
        arr[0] = new ResizableDoubleArray();
        for(int i = 0; i < chromo.length; i++) {
            arr[0].setElement(i, chromo[i]);
        }
        tree.setChromosomes(arr);
        return tree;
    }

    private static List<double[]> getGenerationChromosomes(Path runDirectoryPath, String generation) {
        // Read all lines from log/best.txt
        Path bestTxt = runDirectoryPath.resolve("log").resolve("best.txt");
        List<String> lines = null;
        try {
            lines = Files.readAllLines(bestTxt);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        if (!generation.matches("-?[0-9]+")) {
            return null;
        }

        String chromoLine = null;
        var goalFoundIn = -1;
        for (var line : lines) {
            if (goalFoundIn > 0) {
                goalFoundIn--;
                continue;
            }
            if (goalFoundIn == 0) {
                chromoLine = line;
                break;
            }
            if (line.equals("[ GENERATION " + generation + " ]")) {
                goalFoundIn = 1;
            }
        }
        if (chromoLine == null) {
            return null;
        }

        //Convert to list and return
        return ChromoConversion.multistr2double(chromoLine);
    }
}
