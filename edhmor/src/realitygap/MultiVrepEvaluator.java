package realitygap;

import coppelia.IntW;
import coppelia.remoteApi;
import modules.control.SinusoidalController;
import modules.evaluation.CalculateModulePositions;
import modules.evaluation.VrepCreateRobot;
import modules.evaluation.dynamicFeatures.DynamicFeatures;
import modules.evaluation.dynamicFeatures.DynamicFeaturesEvaluator;
import modules.evaluation.fitness.FitnessFunction;
import modules.evaluation.fitness.FitnessFunctionFactory;
import modules.util.SimulationConfiguration;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;

public class MultiVrepEvaluator {
    private static final ConcurrentHashMap<Long, MultiVrepSimulator> SIMULATOR_MAP = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<Long, Integer> EVALUATOR_MAP = new ConcurrentHashMap<>();
    private static final AtomicInteger NEXT_EVALUATOR_ID = new AtomicInteger(0);
    private static final Logger LOGGER = Logger.getLogger("failogger");

    private final long threadId;
    private final int evaluatorId;
    private final double[] chromosome;

    private int maxRetryAttempts;
    private VrepCreateRobot robot;
    private SinusoidalController controller;
    private FitnessFunction fitnessFunction;
    private DynamicFeaturesEvaluator dFeaturesEval;
    private String vrepScenePath;
    private double maxSimulationTime;
    private double timeStep;

    private MultiVrepSimulator vrepSimulator;
    private remoteApi vrepApi;
    private int clientID;

    public MultiVrepEvaluator(double[] chromosome, String vrepScenePath, double vrepTimeStep) {
        this.threadId = Thread.currentThread().getId();
        this.chromosome = chromosome;
        this.vrepScenePath = vrepScenePath;
        this.timeStep = vrepTimeStep;

        Integer tempEvaluatorId = EVALUATOR_MAP.getOrDefault(threadId, null);
        if (tempEvaluatorId == null) {
            this.evaluatorId = NEXT_EVALUATOR_ID.getAndIncrement();
            EVALUATOR_MAP.put(this.threadId, this.evaluatorId);
        } else {
            this.evaluatorId = tempEvaluatorId;
        }

        this.getSimulationConfigurationParameters();

        vrepSimulator = SIMULATOR_MAP.getOrDefault(threadId, null);

        int tries = 0;
        while (tries < maxRetryAttempts) {
            var success = reloadConfigFromSimulator();
            if (success) {
                break;
            }
            spawnNewVrepSimulator();
            tries++;
        }
    }

    public void spawnNewVrepSimulator() {
        System.out.println("Creating VREP instance...");
        if (vrepSimulator != null) {
            vrepSimulator.stop();
        }
        clientID = -1;
        int attempt = 0;
        while (clientID == -1 && attempt < maxRetryAttempts) {
            vrepSimulator = new MultiVrepSimulator(evaluatorId);
            vrepSimulator.start();
            clientID = vrepSimulator.getClientID();
            attempt++;
        }
        if (vrepSimulator != null) {
            SIMULATOR_MAP.put(threadId, vrepSimulator);
        }
        System.out.println("(" + evaluatorId + ") New VREP instance created. The client ID is " + clientID);
    }

    public double evaluate() {
        boolean success = true;
        double time = 0;

        vrepApi.simxSynchronous(clientID, true);
        // start the simulation:
        vrepApi.simxStartSimulation(clientID, remoteApi.simx_opmode_oneshot_wait);

        this.fitnessFunction.init();
        this.dFeaturesEval.init();

        // Now step a few times:
        int maxIter = (int) (maxSimulationTime / timeStep);
        for (int i = 0; i < maxIter; i++) {
            //Update joint position goals
            controller.updateJoints(time);

            //Send trigger signal for the next step
            int ret = vrepApi.simxSynchronousTrigger(clientID);
            if (ret != remoteApi.simx_error_noerror && ret != remoteApi.simx_error_novalue_flag) {
                System.out.println("(" + evaluatorId + ") Error triggering the simulation; error=" + ret);
                success = false;
                break;
            }
            vrepApi.simxGetPingTime(clientID, new IntW(0)); //blocking - used to ensure simulation step is done
            time += timeStep;

            this.dFeaturesEval.update(time);
            this.fitnessFunction.update(time);
        }
        if (success) {
            this.dFeaturesEval.end();
            this.fitnessFunction.end(time);
        }

        // stop the simulation:
        vrepApi.simxStopSimulation(clientID, remoteApi.simx_opmode_oneshot_wait);

        // Before closing the connection to V-REP, make sure that the last command sent out had time to arrive. You can guarantee this with (for example):
        getVrepPing();

        //Close the scene
        int iter = 0;
        int ret = vrepApi.simxCloseScene(clientID, remoteApi.simx_opmode_oneshot_wait);
        while (ret != remoteApi.simx_return_ok && iter < 3) {
            ret = vrepApi.simxCloseScene(clientID, remoteApi.simx_opmode_oneshot_wait);
            iter++;
        }
        if (ret != remoteApi.simx_return_ok) {
            System.err.println("(" + evaluatorId + ") The scene has not been closed after 3 trials. Latest return code: " + ret);
            success = false;
        }

        if (success) {
            return this.fitnessFunction.getFitness();
        } else {
            return -1;
        }
    }

    private boolean reloadConfigFromSimulator() {
        if (vrepSimulator == null) {
            return false;
        }

        vrepApi = vrepSimulator.getVrepApi();
        clientID = vrepSimulator.getClientID();

        robot = new VrepCreateRobot(vrepApi, clientID, chromosome, "", 1.0, false, false);
        var created = robot.createRobot(vrepScenePath);
        if (!created) {
            return false;
        }

        controller = new SinusoidalController(vrepApi, clientID, robot);
        dFeaturesEval = new DynamicFeaturesEvaluator(vrepApi, clientID, robot);
        fitnessFunction = FitnessFunctionFactory.getFitnessFunction(vrepApi, clientID, robot, dFeaturesEval.getDynamicFeatures());

        return true;
    }


    public DynamicFeatures getDynamicFeatures() {
        return this.dFeaturesEval.getDynamicFeatures();
    }

    public CalculateModulePositions getRobotFeatures() {
        return this.robot.getRobotFeatures();
    }

    private void getSimulationConfigurationParameters() {

        try {
            this.maxSimulationTime = SimulationConfiguration.getMaxSimulationTime();
            this.maxRetryAttempts = SimulationConfiguration.getAttempts();

        } catch (Exception e) {
            LOGGER.severe("Error loading the control parameters of the simulation.");
            System.out.println(e);
            System.exit(-1);
        }

    }

    public int getEvaluatorId() {
        return evaluatorId;
    }

    public void getVrepPing() {
        IntW pingTime = new IntW(0);
        vrepApi.simxGetPingTime(clientID, pingTime);
    }

    public FitnessFunction getFitnessFunction() {
        return this.fitnessFunction;
    }

    public static void killAllSimulators() {
        for (var sim : SIMULATOR_MAP.values()) {
            sim.stop();
        }
        SIMULATOR_MAP.clear();
    }
}