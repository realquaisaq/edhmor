package realitygap;

import modules.evaluation.fitness.PathTravelledFitnessFunction;
import modules.util.ChromoConversion;
import modules.util.SimulationConfiguration;
import realitygap.dto.*;

import javax.vecmath.Vector2d;
import javax.vecmath.Vector3d;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.*;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Main class for running the position test.
 * Takes four arguments:
 *  1) Where to find results of previous runs
 *  2) What class of runs to look for (tuning / gen_test / other)
 *  3) What type of runs to look for (bullet? 1.0fric? 50ms?)
 *  4) Amount of threads to process simulations with (should be set to hyper-threads available * 2)
 *
 * It scans the result-directory for the matching runs, and then starts running the simulations
 * Runs are tested in the other simulations at the same timestep and friction levels.
 * Output goes to output.txt
 */
public class TransferTester {
    private static ExecutorService executorService;

    public static void main(String[] args) throws Exception {
        final String resultsPath = args[0];
        final String dataDirPattern = args[1];
        final String runDirPattern = args[2];
        final String generationToEvaluate = args[3];

        var threadCount = Integer.parseInt(args[4]);
        executorService = Executors.newFixedThreadPool(threadCount);

        class DataProcessingDTO {
            String dataDirectoryName;
            String runDirectoryName;
            Path runDirectoryPath;
        }
        List<DataProcessingDTO> runsToProcess = new ArrayList<>();

        try (DirectoryStream<Path> dataDirectoryStream = Files.newDirectoryStream(new File(resultsPath).toPath(), dataDirPattern)) {
            dataDirectoryStream.forEach(dataDirectoryPath -> {
                String dataDirectoryName = dataDirectoryPath.getFileName().toString();
                try (DirectoryStream<Path> runDirectoryStream = Files.newDirectoryStream(dataDirectoryPath, runDirPattern)) {
                    runDirectoryStream.forEach(runDirectoryPath -> {
                        String runDirectoryName = runDirectoryPath.getFileName().toString();

                        DataProcessingDTO dto = new DataProcessingDTO();
                        dto.dataDirectoryName = dataDirectoryName;
                        dto.runDirectoryName = runDirectoryName;
                        dto.runDirectoryPath = runDirectoryPath;
                        runsToProcess.add(dto);
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }

        ArrayList<RobotEvaluation> robotEvaluations = new ArrayList<>();
        for (DataProcessingDTO dto : runsToProcess) {
            processTheRun(dto.dataDirectoryName, dto.runDirectoryName, dto.runDirectoryPath, robotEvaluations, generationToEvaluate);
        }

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

        File outputFile = new File("output4.txt");
        FileWriter fileWriter = new FileWriter(outputFile, true);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        String newLine = System.getProperty("line.separator");

        int processCounter = 1;
        for (RobotEvaluation robotEvaluation : robotEvaluations) {
            System.out.println(dtf.format(LocalDateTime.now()) + ": Getting (" +
                    robotEvaluation.getDataDirectoryNumber() + ") (" + robotEvaluation.getRunDirectoryName() +
                    ") - process counter: " + processCounter + "/" + robotEvaluations.size());

            bufferedWriter.write(robotEvaluation.getDataDirectoryNumber());
            bufferedWriter.write(' ');
            bufferedWriter.write(robotEvaluation.getRunDirectoryName());
            bufferedWriter.write(' ');
            bufferedWriter.write(Integer.toString(robotEvaluation.getChromosomeCounter()));

            for (Future<SimulationRun> futureNewPositions : robotEvaluation.getFutureSimulationRuns()) {
                SimulationRun newSimulationRun = futureNewPositions.get();
                writeSimulationRunToFile(newSimulationRun, bufferedWriter);
            }

            bufferedWriter.write(newLine);
            bufferedWriter.flush();

            processCounter++;
        }

        bufferedWriter.flush();
        bufferedWriter.close();

        MultiVrepEvaluator.killAllSimulators();

        System.out.println("Finished!");
        System.exit(0);
    }

    public static void processTheRun(String dataDirectoryName, String runDirectoryName,
                                     Path runDirectoryPath, List<RobotEvaluation> robotEvaluations,
                                     String generationToEvaluate) {
        String vrepScenePath = getScenePath(runDirectoryName);

        int dataNumberUnderscoreIndex = dataDirectoryName.lastIndexOf('_');
        String dataNumberString = dataDirectoryName.substring(dataNumberUnderscoreIndex + 1);
        int dataNumber = Integer.parseInt(dataNumberString);

        int engineNameUnderscoreIndex1 = runDirectoryName.indexOf('_');
        int engineNameUnderscoreIndex2 = runDirectoryName.indexOf('_', engineNameUnderscoreIndex1 + 1);
        String engineName = runDirectoryName.substring(engineNameUnderscoreIndex1 + 1, engineNameUnderscoreIndex2);

        int msUnderscoreIndex = runDirectoryName.lastIndexOf('_');
        String msString = runDirectoryName.substring(msUnderscoreIndex + 1);
        String timeStepString = msString.substring(0, msString.length() - 2);
        double timeStep = Integer.parseInt(timeStepString) / 1000D;
        int frictionUnderscoreIndex = runDirectoryName.substring(0, msUnderscoreIndex).lastIndexOf('_');
        String frictionString = runDirectoryName.substring(frictionUnderscoreIndex + 1, msUnderscoreIndex);
        double friction = Double.parseDouble(frictionString.substring(0, frictionString.length() - 4));

        VrepSceneConfig sceneConfig = new VrepSceneConfig(vrepScenePath, engineName, friction, timeStep);
        List<double[]> chromosomes = getGenerationChromosomes(runDirectoryPath, generationToEvaluate);

        if (chromosomes == null) {
            System.err.println("Failed to evaluate: " + dataDirectoryName + "/" + runDirectoryName);
            return;
        }

        //setup configs
        var transferEngine = getTransferEngine(runDirectoryName);
        var testEngines = new ArrayList<String>();

        //testEngines.add(engineName); //skip original
        if (transferEngine != null) {
            testEngines.add(transferEngine);
        }

        testEngines.addAll(Stream.of("bullet", "ode", "newton")
                .filter(s -> !s.equals(engineName) && !s.equals(transferEngine))
                .collect(Collectors.toList()));

        //testEngines now contains the original first, the transfer engine second (if there is one), and the rest after.

        List<Double> testFrictions = List.of(friction);
        List<Double> testTimeSteps = List.of(timeStep);

        // Run for each chromosome
        int chromosomeCounter = 1;
        for (double[] chromosome : chromosomes) {
            List<Future<SimulationRun>> futureSimulationRuns = new ArrayList<>();
            var originalPositions = evaluateFutureRun(chromosome, sceneConfig, 0);
            futureSimulationRuns.add(originalPositions);

            //We want 5 samples from ODE - the others we only need 1
            if (sceneConfig.getEngineName().equals("ode")) {
                for (int rerun = 1; rerun < 5; rerun++) {
                    var originalPositionsODE = evaluateFutureRun(chromosome, sceneConfig, rerun);
                    futureSimulationRuns.add(originalPositionsODE);
                }
            }

            // Test the other simulators
            List<VrepSceneConfig> sceneConfigs = VrepSceneConfigGenerator.generate(testEngines, testFrictions, testTimeSteps);
            for (VrepSceneConfig config : sceneConfigs) {
                Future<SimulationRun> futureSimulationRun = evaluateFutureRun(chromosome, config, 0);
                futureSimulationRuns.add(futureSimulationRun);

                //We want 5 samples from ODE - the others we only need 1
                if (config.getEngineName().equals("ode")) {
                    for (int rerun = 1; rerun < 5; rerun++) {
                        var futureSimulationRunODE = evaluateFutureRun(chromosome, config, rerun);
                        futureSimulationRuns.add(futureSimulationRunODE);
                    }
                }
            }

            RobotEvaluation run = new RobotEvaluation(dataDirectoryName, runDirectoryName, chromosomeCounter,
                    futureSimulationRuns);
            robotEvaluations.add(run);

            chromosomeCounter++;
        }
    }

    private static String getScenePath(String runDirectoryName) {
        var toIndex = runDirectoryName.indexOf("_to_");
        if (toIndex == -1) {
            return "scenes/edhmor/" + runDirectoryName + ".ttt";
        }
        else {
            var scenePart1 = runDirectoryName.substring(0, toIndex);
            var engine2EndIndex = runDirectoryName.indexOf("_", toIndex + 5);
            var scenePart2 = runDirectoryName.substring(engine2EndIndex + 1);
            return "scenes/edhmor/" + scenePart1 + "_" + scenePart2 + ".ttt";
        }
    }

    private static String getTransferEngine(String runDirectoryName) {
        var toIndex = runDirectoryName.indexOf("_to_");
        if (toIndex == -1) {
            return null;
        }
        else {
            var transferEnginePosEnd = runDirectoryName.indexOf("_", toIndex + 5);
            return runDirectoryName.substring(toIndex + 4, transferEnginePosEnd);
        }
    }

    private static void writeSimulationRunToFile(SimulationRun simulationRun, BufferedWriter bufferedWriter) throws IOException {
        VrepSceneConfig sceneConfig = simulationRun.getSceneConfig();
        var positionCount = simulationRun.getPositions().size();
        var errorCount = simulationRun.getErrors().size();
        var moduleCount = simulationRun.getModuleCount();
        var df = NumberFormat.getNumberInstance(Locale.US);
        df.setMaximumFractionDigits(5);
        df.setGroupingUsed(false);

        bufferedWriter.write(" # ");
        bufferedWriter.write(sceneConfig.getEngineName());
        bufferedWriter.write(' ');
        bufferedWriter.write(Integer.toString(simulationRun.getRerunIndex()));
        bufferedWriter.write(' ');
        bufferedWriter.write(Integer.toString(positionCount));
        bufferedWriter.write(' ');
        bufferedWriter.write(Integer.toString(errorCount));
        bufferedWriter.write(' ');
        bufferedWriter.write(Integer.toString(moduleCount));
        bufferedWriter.write(' ');
        bufferedWriter.write(df.format(simulationRun.getFitness()));

        for (Map.Entry<Double, Vector3d> entry : simulationRun.getPositions().entrySet()) {
            var time = entry.getKey();
            var pos = entry.getValue();
            bufferedWriter.write(' ');
            bufferedWriter.write(df.format(time));
            bufferedWriter.write(' ');
            bufferedWriter.write(df.format(pos.x));
            bufferedWriter.write(' ');
            bufferedWriter.write(df.format(pos.y));
            bufferedWriter.write(' ');
            bufferedWriter.write(df.format(pos.z));
        }

        bufferedWriter.write(" %");

        for (Map.Entry<Double, List<Vector2d>> entry : simulationRun.getErrors().entrySet()) {
            var time = entry.getKey();
            var errors = entry.getValue();
            bufferedWriter.write(' ');
            bufferedWriter.write(df.format(time));
            for (var error : errors) {
                bufferedWriter.write(' ');
                bufferedWriter.write(df.format(error.x));
                bufferedWriter.write(' ');
                bufferedWriter.write(df.format(error.y));
            }
        }
    }

    private static List<double[]> getGenerationChromosomes(Path runDirectoryPath, String generation) {
        // Read all lines from log/best.txt
        Path bestTxt = runDirectoryPath.resolve("log").resolve("best.txt");
        List<String> lines = null;
        try {
            lines = Files.readAllLines(bestTxt);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        if (generation.equals("latest")) {
            return getBestChromosomes(lines);
        }
        else if (!generation.matches("-?[0-9]+")) {
            return null;
        }

        String chromoLine = null;
        var goalFoundIn = -1;
        for (var line : lines) {
            if (goalFoundIn > 0) {
                goalFoundIn--;
                continue;
            }
            if (goalFoundIn == 0) {
                chromoLine = line;
                break;
            }
            if (line.equals("[ GENERATION " + generation + " ]")) {
                goalFoundIn = 1;
            }
        }
        if (chromoLine == null) {
            return null;
        }

        //Convert to list and return
        return ChromoConversion.multistr2double(chromoLine);
    }

    private static List<double[]> getBestChromosomes(List<String> lines) {
        //Find the latest generation chromosome
        Collections.reverse(lines);
        String bestLine = null;
        for (String line : lines) {
            if (line == null || line.trim().isEmpty()) {
                continue;
            }
            if (line.charAt(0) == '[' && line.charAt(1) == '[') {
                bestLine = line;
                break;
            }
        }
        if (bestLine == null) {
            return null;
        }

        //Convert to list and return
        return ChromoConversion.multistr2double(bestLine);
    }

    public static Future<SimulationRun> evaluateFutureRun(double[] values, VrepSceneConfig sceneConfig, int rerunIndex) {
        Future<SimulationRun> future = executorService.submit(() -> {
            //Setup simulation
            double evaluatedFitness = -1;
            MultiVrepEvaluator evaluator;
            var attempts = 0;
            var maxRetryAttempts = SimulationConfiguration.getAttempts();

            do {
                attempts++;
                evaluator = new MultiVrepEvaluator(values, sceneConfig.getScenePath(), sceneConfig.getTimeStep());
                var evaluatorId = evaluator.getEvaluatorId();

                //Run simulation
                System.out.println("(" + evaluatorId + ") Evaluating " + sceneConfig.getEngineName() + " " + sceneConfig.getFriction() + " " +
                        sceneConfig.getTimeStep());
                evaluatedFitness = evaluator.evaluate();

                //Handle error in simulation by rerunning simulation
                if (evaluatedFitness == -1) {
                    System.out.println("(" + evaluatorId + ") Failed to evaluate. Restarting VREP..");
                    evaluator.spawnNewVrepSimulator();
                }
            } while (evaluatedFitness == -1 && attempts < maxRetryAttempts);

            //Get positions
            var fitnessFunction = evaluator.getFitnessFunction();
            var pathTravelledFitnessFunction = (PathTravelledFitnessFunction) fitnessFunction; //assume it is path travelled function
            var positions = pathTravelledFitnessFunction.getPositions();
            var errors = pathTravelledFitnessFunction.getErrors();
            var fitness = pathTravelledFitnessFunction.getFitness();
            var moduleCount = evaluator.getRobotFeatures().getnModules();

            //Return
            return new SimulationRun(sceneConfig, positions, errors, fitness, moduleCount, rerunIndex);
        });
        return future;
    }
}
