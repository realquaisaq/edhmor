package realitygap;

import realitygap.dto.VrepSceneConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class VrepSceneConfigGenerator {

    public static List<VrepSceneConfig> generate(List<String> engines, List<Double> frictions, List<Double> timeSteps) {
        List<VrepSceneConfig> configs = new ArrayList<>(engines.size() * frictions.size() * timeSteps.size());

        for (String engine : engines) {
            for (Double friction : frictions) {
                for (Double timeStep : timeSteps) {
                    String frictionString = String.format(Locale.US, "%.1f", friction);
                    String timeStepString = String.format("%d", (int)Math.floor(timeStep * 1000D));
                    String scenePath = "scenes/edhmor/emerge_" + engine + "_" + frictionString + "fric_" + timeStepString + "ms.ttt";
                    configs.add(new VrepSceneConfig(scenePath, engine, friction, timeStep));
                }
            }
        }

        return configs;
    }
}
