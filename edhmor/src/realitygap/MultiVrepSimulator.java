package realitygap;

import coppelia.remoteApi;
import modules.evaluation.VrepSimulator;
import modules.util.SimulationConfiguration;
import org.apache.commons.lang.SystemUtils;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MultiVrepSimulator {
    private static final AtomicInteger nextPort;

    private final int maxRetryAttempts;
    private final int evaluatorId;

    private int port;
    private Process simulator = null;
    private int clientID;
    private remoteApi vrepApi = null;
    private boolean guiOn = false;

    static {
        nextPort = new AtomicInteger(SimulationConfiguration.getVrepStartingPort());
    }

    public MultiVrepSimulator(int evaluatorId) {
        this.evaluatorId = evaluatorId;
        this.port = nextPort.getAndIncrement();
        this.maxRetryAttempts = SimulationConfiguration.getAttempts();
    }

    public void start() {
        //Try to connect to an existing open VrepSimulator
        connect2Vrep();
        if (clientID == -1) {
            //Simulator not found or not responding, launch a new one
            System.out.println("(" + evaluatorId + ") Failed connecting to remote API server on port " + port);
            stop();
            port = nextPort.getAndIncrement();
            launchVrep();
            connect2Vrep();
        }

        if (clientID == -1){
            System.out.println("(" + port + ") Failed connecting to remote API server on port " + port);
            System.out.println("(" + port + ") Program ending");
            stop();
            return;
        }
        else {
            System.out.println("(" + evaluatorId + ") Simulator launched and connected.");
        }

        vrepApi.simxSynchronous(clientID, true);
    }

    public void launchVrep(){
        if (simulator == null) {
            List<String> processArguments = new ArrayList<>();
            String vrepInstallPath = System.getenv("VREP_HOME");

            if (SystemUtils.IS_OS_WINDOWS) {
                Path vrepPath = Paths.get(vrepInstallPath, "vrep.exe");
                processArguments.add(vrepPath.toString());
            } else {
                //SystemUtils.IS_OS_LINUX
                processArguments.add("stdbuf");
                processArguments.add("-o0");
                processArguments.add("-e0");
                processArguments.add("-i0");
                if (SimulationConfiguration.isUseSingularity()) {
                    processArguments.add("singularity");
                    processArguments.add("exec");
                    processArguments.add(SimulationConfiguration.getSingularityPath());
                }


                //Now, we use this line in the .bashrc file
                //export QT_QPA_PLATFORM=offscreen
                //Therefore, we not need to use xvfb-run
//                if (SimulationConfiguration.isUseMPI()) {
//                    processArguments.add("xvfb-run");
//                    processArguments.add("-a");
//                }
                //We call directly vrep and not vrep.sh
                //Be sure that the VREP_HOME is added to LD_LIBRARY_PATH
                processArguments.add(vrepInstallPath + "/vrep");
            }

            if(!guiOn){
                processArguments.add("-h");
            }
            processArguments.add("-gREMOTEAPISERVERSERVICE_" + port + "_FALSE_TRUE");

            System.out.println(" (" + evaluatorId + ") vrepPath: " + vrepInstallPath);
            System.out.println(" (" + evaluatorId + ") Setting the port to: " + port);

            /*Initialize a v-rep simulator based on the starNumber parameter */
            try {
                ProcessBuilder qq = new ProcessBuilder(processArguments);//, "-h"/home/rodr/EvolWork/Modular/Maze/MazeBuilderR01.ttt");

                qq.directory(new File(vrepInstallPath));
                qq.redirectErrorStream(true); //Error and std output redirected to the same pipe

                simulator = qq.start();
                Thread.sleep(4000); //wait for the vrepApi simulators
            } catch (IOException e) {
                System.out.println(e.toString());
            } catch (InterruptedException e) {
                System.out.println(e.toString());
            }
        }
    }

    public void disconnect(){
        vrepApi.simxFinish(clientID);
    }

    public void connect2Vrep(){
        System.out.println("(" + evaluatorId + ") Connect2Vrep");
        vrepApi = new remoteApi();
        System.out.println("(" + evaluatorId + ") New RemoteApi loaded");
        clientID = vrepApi.simxStart("127.0.0.1", port, true, true, 5000, 2);
        System.out.println("(" + evaluatorId + ") Vrep connection started: (clientId: " + clientID + ", port: " + port + ")");
    }

    public void stop() {
        System.out.println(" (" + evaluatorId + ") Stopping vrep at port : " + port);
        // First close the connection to V-REP:

        int exitStatus = -1000;
        if (simulator != null) {
            try {
                if(simulator.toHandle().destroyForcibly()){
                    System.out.println(" (" + evaluatorId + ") Process forcibly destroyed correctly vrep at port : " + port);
                }else{
                    System.out.println(" (" + evaluatorId + ") Process did NOT forcibly destroy vrep at port : " + port);
                }
                simulator.destroy();
                simulator.getErrorStream().close();
                simulator.getInputStream().close();
                simulator.getOutputStream().close();
                if (simulator != null) {
                    try {
                        System.out.println(" (" + evaluatorId + ") Waiting for process to finish");
                        exitStatus = simulator.waitFor();
                        System.out.println(" (" + evaluatorId + ") Process finished");
                    } catch (InterruptedException ex) {
                        Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println(" (" + evaluatorId + ") Vrep finished with code: " + exitStatus);

            try {
                Thread.sleep(2000); //wait for the vrepApi simulators
            } catch (InterruptedException ex) {
                Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (clientID != -1) {
            vrepApi.simxFinish(clientID);
        }
        System.out.println(" (" + evaluatorId + ") Vrep process has been stopped");
        simulator = null;
    }

    public int getClientID() {
        return clientID;
    }

    public remoteApi getVrepApi() {
        return vrepApi;
    }
}
