/* 
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.jeaf.application.edhmor;

//import es.udc.gii.common.eaf.algorithm.fitness.ObjectiveFunction;

import es.udc.gii.common.eaf.problem.objective.ObjectiveFunction;
import modules.evaluation.CalculateModulePositions;
import modules.evaluation.VrepEvaluator;
import modules.evaluation.dynamicFeatures.DynamicFeatures;
import modules.evaluation.fitness.DistanceTravelledFitnessFunction;
import modules.util.SimulationConfiguration;

import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fai
 */
public class TransferabilityObjectiveFunction extends ObjectiveFunction {

    public TransferabilityObjectiveFunction() {
    }
    private int nEval = 1;
    
    public static final int OBJECTIVE_FUNCTION = 0;
    public static final int OBJECTIVE_N_MODULES = 1;
    public static final int OBJECTIVE_FUNCTION_WORLD_1 = 2;
    public static final int OBJECTIVE_BROCKEN_CONN_WORLD_1 = 3;
    

    public List<Double> evaluate(double[] values) {
        List<Double> objectives = new ArrayList<Double>();
        var finalFitness = 0D;

        var vrepScene = SimulationConfiguration.getVrepScenePath();
        var vrepTransferScene = SimulationConfiguration.getVrepTransferScenePath();
        var transferGapThreshold = SimulationConfiguration.getTransferGapThreshold();
        var transferGapWeight = SimulationConfiguration.getTransferGapWeight();
        var transferFitnessWeight = SimulationConfiguration.getTransferFitnessWeight();

        var rawEvaluator = new VrepEvaluator(values, null, 1, false, vrepScene);
        var rawFitness = rawEvaluator.evaluate();
        while (rawFitness == -1) {
            rawFitness = rawEvaluator.evaluate();
        }
        var rawFitnessFunction = (DistanceTravelledFitnessFunction)rawEvaluator.getFitnessFunction();
        var rawFinalPosition = rawFitnessFunction.getFinalPosition();

        var transferEvaluator = new VrepEvaluator(values, null,1, false, vrepTransferScene);
        var transferFitness = transferEvaluator.evaluate();
        while (transferFitness == -1) {
            transferFitness = transferEvaluator.evaluate();
        }
        var transferFitnessFunction = (DistanceTravelledFitnessFunction)transferEvaluator.getFitnessFunction();
        var transferFinalPosition = transferFitnessFunction.getFinalPosition();

        var diffFinalPosition = new Vector3d(rawFinalPosition);
        diffFinalPosition.sub(transferFinalPosition);

        var diffLength = diffFinalPosition.length();
        if (diffLength < transferGapThreshold) {
            finalFitness = rawFitness * transferFitnessWeight - diffLength * transferGapWeight;
        }
        else {
            finalFitness = 0D;
        }

        System.out.println("Evaluation " + nEval + " of transfer from scene " + vrepScene + " to " + vrepTransferScene + ". Raw fitness: " + rawFitness + ", diffLength: " + diffLength + ", Final fitness:" + finalFitness);

        this.nEval++;

        var nModules = rawEvaluator.getRobotFeatures().getnModules();
        var brokenModules = (double)rawEvaluator.getDynamicFeatures().getBrokenConnections();

        //Put the overall fitness
        objectives.add(OBJECTIVE_FUNCTION, finalFitness);
        objectives.add(OBJECTIVE_N_MODULES, (double) nModules);
        objectives.add(OBJECTIVE_FUNCTION_WORLD_1, finalFitness);
        objectives.add(OBJECTIVE_BROCKEN_CONN_WORLD_1, brokenModules);
        return objectives;
    }

    public void reset() {
    }
}
