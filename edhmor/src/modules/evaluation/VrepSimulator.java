/*
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2016 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.evaluation;

import coppelia.remoteApi;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modules.util.SimulationConfiguration;
import mpi.MPI;
import org.apache.commons.lang.SystemUtils;

/**
 * VrepSimulator.java Created on 27/03/2016
 *
 * @author Andres Faiña <anfv  at itu.dk>
 */
public class VrepSimulator {

    Process simulator = null;
    StreamGobbler errorGobbler = null;
    StreamGobbler outputGobbler = null;
    private int rank = 0, clientID;
    private remoteApi vrepApi = null;
    private int port = SimulationConfiguration.getVrepStartingPort();
    private int attempt = 0;
    private boolean guiOn = false;
    public void start() {
        this.start(19997);
    }

    public void start(int jobId) {
        if (SimulationConfiguration.isUseMPI()) {
            rank = MPI.COMM_WORLD.Rank();
        }
        int initPort = 4;//(int) (jobId % 316);
        //SimulationConfiguration.setVrepStartingPort(initPort * 144 + 45000);
        port = SimulationConfiguration.getVrepStartingPort();
        if (SimulationConfiguration.isUseMPI()) {
            port += MPI.COMM_WORLD.Rank();
        }
        
        //Try to connect to an existing open VrepSimulator
        connect2Vrep();
        if(clientID == -1){
            //Simulator not found or not responding, launch a new one
            System.out.println(rank + "Simulator not found or not responding, launch a new one. Attempt: " + attempt);
            stop();
            launchVrep(jobId);
            connect2Vrep();
        }
        
        
        while (clientID == -1 && attempt < SimulationConfiguration.getAttempts()) {
            System.out.println("(" + port + ")Failed connecting to remote API server on port " + port +  "; rank: " + rank + "; attempt: " + attempt);
            int nSimulators = 1;
            if (SimulationConfiguration.isUseMPI()) {
                nSimulators = MPI.COMM_WORLD.Size();
            }
            port+=nSimulators*(attempt+1);
            System.out.println("(" + port + ")Restarting remote API server on port " + port +  "; rank: " + rank + "; attempt: " + attempt);
            
            stop();
            launchVrep(jobId);
            connect2Vrep();
            attempt++;
        }
        if(clientID == -1){
            System.out.println("(" + port + ")Failed connecting to remote API server on port " + port +  "; rank: " + rank + " after " + SimulationConfiguration.getAttempts() + " attempts");
            System.out.println("(" + port + ")Program ended");
            MPI.COMM_WORLD.Abort(-1);//Try to close all the programs
            pkillVrep();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            pkillJava();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.exit(-1);
        }else{
            System.out.println(rank + ": Simulator launched and connected in attempt: " + attempt);
        }
        
        vrepApi.simxSynchronous(clientID, true);
    }
    
    public void launchVrep(int jobId){
        
        if (simulator == null) {
            List<String> processArguments = new ArrayList<String>();
            String vrepInstallPath = System.getenv("VREP_HOME");

            if (SystemUtils.IS_OS_WINDOWS) {
                Path vrepPath = Paths.get(vrepInstallPath, "vrep.exe");
                processArguments.add(vrepPath.toString());
            } else {
                //SystemUtils.IS_OS_LINUX
                processArguments.add("stdbuf");
                processArguments.add("-o0");
                processArguments.add("-e0");
                processArguments.add("-i0");
                if (SimulationConfiguration.isUseSingularity()) {
                    processArguments.add("singularity");
                    processArguments.add("exec");
                    processArguments.add(SimulationConfiguration.getSingularityPath());
                }

                
                //Now, we use this line in the .bashrc file
                //export QT_QPA_PLATFORM=offscreen
                //Therefore, we not need to use xvfb-run
//                if (SimulationConfiguration.isUseMPI()) {
//                    processArguments.add("xvfb-run");
//                    processArguments.add("-a");
//                }
                //We call directly vrep and not vrep.sh
                //Be sure that the VREP_HOME is added to LD_LIBRARY_PATH
                processArguments.add(vrepInstallPath + "/vrep");
            }

            if(!guiOn){
                processArguments.add("-h");
            }
            processArguments.add("-gREMOTEAPISERVERSERVICE_" + port + "_FALSE_TRUE");

            System.out.println("vrepPath (" + rank + "): " + vrepInstallPath);
            System.out.println(" (" + rank + ")Setting the port to: " + port);

            /*Initialize a v-rep simulator based on the starNumber parameter */
            try {

                ProcessBuilder qq = new ProcessBuilder(processArguments);//, "-h"/home/rodr/EvolWork/Modular/Maze/MazeBuilderR01.ttt");

                qq.directory(new File(vrepInstallPath));
                qq.redirectErrorStream(true); //Error and std output redirected to the same pipe

                //Vrep error and standart output redirected to the output of the 
                //program. To redirect to a file use these line 
                //File log = new File("vrep_output.txt");
                //qq.redirectOutput(ProcessBuilder.Redirect.appendTo(log));
//                qq.redirectOutput(ProcessBuilder.Redirect.INHERIT);
                simulator = qq.start();
//                outputGobbler = new StreamGobbler(simulator.getInputStream(), "VREP_" + port + "_OUTPUT");
//                errorGobbler = new StreamGobbler(simulator.getErrorStream(), "VREP_" + port + "_ERROR");
                // kick them off
//                errorGobbler.start();
//                outputGobbler.start();

//            if (MPI.COMM_WORLD.Rank() == 0) {
                Thread.sleep(4000); //wait for the vrepApi simulators
//            }

            } catch (IOException e) {
                System.out.println(e.toString());
            } catch (InterruptedException e) {
                System.out.println(e.toString());
            }
        }
    }
    public void disconnect(){
    	vrepApi.simxFinish(clientID);
    }
    
    public void connect2Vrep(){
        System.out.println(rank + ": Connect2Vrep");
        vrepApi = new remoteApi();
        System.out.println(rank + ": New RemoteApi loaded");
        vrepApi.simxFinish(-1); // just in case, close all opened connections
        System.out.println(rank + ": Finished all Vrep Connections");
        clientID = vrepApi.simxStart("127.0.0.1", port, true, true, 5000, 2);
        System.out.println(rank + ": Vrep connection started: (clientId: " + clientID + ")");
    }

    public void stop() {
        
        System.out.println(" (" + rank + ")Stopping vrep at port : " + port);
        // First close the connection to V-REP:	
        
                
        int exitStatus = -1000;
        if (simulator != null) {
            try {
                if(simulator.toHandle().destroyForcibly()){
                    System.out.println(" (" + rank + ")Process forcibly destroyed correctly vrep at port : " + port);
                }else{
                    System.out.println(" (" + rank + ")Process did NOT forcibly destroy vrep at port : " + port);
                }
                simulator.destroy();
                simulator.getErrorStream().close();
                simulator.getInputStream().close();
                simulator.getOutputStream().close();
                if (simulator != null) {
                    try {
                        System.out.println(" (" + rank + ") Waiting for process to finish");
                        exitStatus = simulator.waitFor();
                        System.out.println(" (" + rank + ") Process finished");
                        if (!SystemUtils.IS_OS_WINDOWS) {

//                            killUnixProcess(simulator);
                        }

                    } catch (InterruptedException ex) {
                        Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println(" (" + rank + ")Vrep finished with code: " + exitStatus);
//        if (!SystemUtils.IS_OS_WINDOWS && MPI.COMM_WORLD.Rank() == 0) {
////            System.out.println(" (" + rank + ")Ps ux 2: ");
////            getPsUX();
//        }

            try {
                Thread.sleep(2000); //wait for the vrepApi simulators
            } catch (InterruptedException ex) {
                Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        vrepApi.simxFinish(clientID);
        System.out.println(" (" + rank + ")Vrep communicattion thread has finished ");
        simulator = null;
    }

    public long getUnixPID(Process process) throws Exception {
        
        long pid = process.pid();
        System.out.println(process.getClass().getName() + ", pid: " + pid);
        return pid;
//        if (process.getClass().getName().equals("java.lang.UNIXProcess")) {
//            Class cl = process.getClass();
//            Field field = cl.getDeclaredField("pid");
//            field.setAccessible(true);
//            Object pidObject = field.get(process);
//            return (Integer) pidObject;
//        } else {
//            throw new IllegalArgumentException("Needs to be a UNIXProcess");
//        }
    }

    public void killUnixProcess(Process process) throws Exception {
        long pid = getUnixPID(process);
        System.out.println(" (" + rank + ")PID of the vrep is: " + pid);
        //return Runtime.getRuntime().exec("pkill -TERM -P " + pid).waitFor();
        //return Runtime.getRuntime().exec("kill -- -$(ps -o pgid= "+ pid +" | grep -o '[0-9]*')").waitFor();

//        System.out.println(" (" + rank + ")Stopping vrep at port : " + port);
//        if(process.toHandle().destroy())
//        {
//            System.out.println(" (" + rank + ")Process destroyed correctly vrep at port : " + port);
//        }else{
            if(process.toHandle().destroyForcibly()){
                System.out.println(" (" + rank + ")Process forcibly destroyed correctly vrep at port : " + port);
            }else{
                System.out.println(" (" + rank + ")Process did NOT forcibly destroy vrep at port : " + port);
            }
//        }
    }
    
    public void pkillVrep(){
        try {
            try {
                Runtime.getRuntime().exec("pkill vrep" ).waitFor();
            } catch (InterruptedException ex) {
                Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
            }
            Thread.sleep(200);
        } catch (IOException ex) {
            Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
        }
        ;
    }
    
    public void pkillJava(){
        try {
            try {
                Runtime.getRuntime().exec("pkill java" ).waitFor();
            } catch (InterruptedException ex) {
                Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
            }
            Thread.sleep(200);
        } catch (IOException ex) {
            Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
        }
        ;
    }
    
    public int killVrep() throws Exception {
        System.out.println(" (" + rank + ")Stopping vrep at port : " + port);
        return Runtime.getRuntime().exec("./removeVrep.sh "+ port ).waitFor();
    }

    public int killall(String command) throws Exception {
        return Runtime.getRuntime().exec("killall " + command).waitFor();
    }

    public void getPsUX() {

        String str = "";
        try {

            List<String> processArguments = new ArrayList<String>();
            processArguments.add("ps");
            processArguments.add("ux");
            ProcessBuilder qq = new ProcessBuilder(processArguments);
            Process p = qq.start();
            p.waitFor();
            BufferedReader brStdOut = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader brStdErr = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            String strErr = "", strOut = "";
            System.out.println("\n");
            while (brStdOut.ready()) {
                strOut = "(" + rank + ")";
                strOut += brStdOut.readLine();
                System.out.println(strOut);
            }
            System.out.println("\n");
            while (brStdErr.ready()) {
                strErr= "(" + rank + ")";
                strErr += brStdErr.readLine();
                System.err.println(strOut);
            }
            //str = strOut + strErr;
            brStdOut.close();
            brStdErr.close();

        } catch (IOException ex) {
            Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(VrepSimulator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getClientID() {
        return clientID;
    }

    public remoteApi getVrepApi() {
        return vrepApi;
    }

    public void setGuiOn(boolean guiOn) {
        this.guiOn = guiOn;
    }
    

}

/*Code taken from http://www.javaworld.com/article/2071275/core-java/when-runtime-exec---won-t.html*/
class StreamGobbler extends Thread {

    InputStream is;
    String type;

    StreamGobbler(InputStream is, String type) {
        this.is = is;
        this.type = type;
    }

    @Override
    public void run() {
        try {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line = null;
            for (;;) {
                while ((line = br.readLine()) != null) {
                    System.out.println(type + ">" + line);
                }
                try {
                    Thread.sleep(2);
                } catch (InterruptedException ex) {
                    Logger.getLogger(StreamGobbler.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
