package modules.evaluation.fitness;

import coppelia.*;
import modules.ModuleSetFactory;
import modules.evaluation.VrepCreateRobot;

import javax.vecmath.Vector2d;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Same as DistanceTravelledFitnessFunction, except it keeps track of position at all times
 */
public class PathTravelledFitnessFunction extends DistanceTravelledFitnessFunction {
    //private final int positionUpdateInterval = 1; //update once per second
    private final int positionUpdateInterval = 0; //update once per tick

    private TreeMap<Double, Vector3d> positions;
    private TreeMap<Double, List<Vector2d>> errors;
    private double lastPositionUpdate;

    public PathTravelledFitnessFunction(remoteApi vrep, int clientID, VrepCreateRobot robot) {
        super(vrep, clientID, robot);
        positions = new TreeMap<>();
        errors = new TreeMap<>();
        lastPositionUpdate = 0;
    }

    @Override
    public void init() {
        var oldInitialPos = initialPos;
        super.init();
        if (oldInitialPos != initialPos) {
            positions.put(0D, initialPos);
        }
        StartStream();
    }

    @Override
    public void update(double time) {
        var oldInitialPos = initialPos;
        var oldFinalPos = finalPos;
        super.update(time);

        if (oldInitialPos != initialPos) {
            errors.put(time, getErrorsFromSimulation());
            positions.put(time, initialPos);
            lastPositionUpdate = time;
        }

        if (oldFinalPos != finalPos) {
            errors.put(time, getErrorsFromSimulation());
            positions.put(time, finalPos);
            lastPositionUpdate = time;
        }

        if (initialPos != null && time >= lastPositionUpdate + positionUpdateInterval) {
            var position = super.getPose();
            lastPositionUpdate = time;
            positions.put(time, position);
            errors.put(time, getErrorsFromSimulation());
        }
    }

    @Override
    public void end(double time) {
        var oldFinalPos = finalPos;
        super.end(time);

        if (oldFinalPos != finalPos) {
            errors.put(time, getErrorsFromSimulation());
            positions.put(time, finalPos);
        }
        EndStream();
    }

    private List<Vector2d> getErrorsFromSimulation() {
        var errors = new ArrayList<Vector2d>();
        var handles = robot.getModuleHandlers();
        for (var handle : handles) {
            int jointHandle = handle + 2;
            var output = new FloatW(0f);
            var error = new Vector2d();
            vrep.simxGetObjectFloatParameter(clientID, jointHandle, remoteApi.sim_jointfloatparam_error_pos, output, remoteApi.simx_opmode_buffer);
            error.x = output.getValue();
            vrep.simxGetObjectFloatParameter(clientID, jointHandle, remoteApi.sim_jointfloatparam_error_angle, output, remoteApi.simx_opmode_buffer);
            error.y = output.getValue();
            errors.add(error);
        }
        return errors;
    }

    @Override
    protected Vector3d getBasePose() {
        int baseHandle = robot.getModuleHandlers().get(0) + 1;
        FloatWA position = new FloatWA(3);
        vrep.simxGetObjectPosition(clientID, baseHandle, -1 /*Absolute position*/, position, remoteApi.simx_opmode_buffer);
        return new Vector3d(position.getArray()[0],position.getArray()[1], position.getArray()[2]);
    }

    @Override
    protected Vector3d getCenterOfMass() {
        var handles = robot.getModuleHandlers();
        int[] moduleTypes = robot.getModuleType();
        var moduleSet = ModuleSetFactory.getModulesSet();

        double x = 0, y = 0, z = 0, robotMass = 0;
        for (var i = 0; i < handles.size(); i++) {
            var handle = handles.get(i);
            var actuatorHandle = handle + 1;
            var pos = new FloatWA(3);
            var type = moduleTypes[i];
            var mass = moduleSet.getModulesMass(type);
            vrep.simxGetObjectPosition(clientID, actuatorHandle, -1 /* absolute position */, pos, remoteApi.simx_opmode_buffer);

            x += pos.getArray()[0] * mass;
            y += pos.getArray()[1] * mass;
            z += pos.getArray()[2] * mass;
            robotMass += mass;
        }

        Vector3d com = new Vector3d(x / robotMass, y / robotMass, z / robotMass);
        //System.out.println("Center of mass coordinates: " + com.toString());
        return com;
    }

    private void StartStream() {
        var handles = robot.getModuleHandlers();
        for (var handle : handles) {
            int actuatorHandle = handle + 1;
            int jointHandle = handle + 2;
            vrep.simxGetObjectFloatParameter(clientID, jointHandle, remoteApi.sim_jointfloatparam_error_pos, new FloatW(0f), remoteApi.simx_opmode_streaming);
            vrep.simxGetObjectFloatParameter(clientID, jointHandle, remoteApi.sim_jointfloatparam_error_angle, new FloatW(0f), remoteApi.simx_opmode_streaming);
            vrep.simxGetObjectPosition(clientID, actuatorHandle, -1 /* absolute position */, new FloatWA(3), remoteApi.simx_opmode_streaming);
        }
    }

    private void EndStream() {
        var handles = robot.getModuleHandlers();
        for (var handle : handles) {
            int actuatorHandle = handle + 1;
            int jointHandle = handle + 2;
            vrep.simxGetObjectFloatParameter(clientID, jointHandle, remoteApi.sim_jointfloatparam_error_pos, new FloatW(0f), remoteApi.simx_opmode_discontinue);
            vrep.simxGetObjectFloatParameter(clientID, jointHandle, remoteApi.sim_jointfloatparam_error_angle, new FloatW(0f), remoteApi.simx_opmode_discontinue);
            vrep.simxGetObjectPosition(clientID, actuatorHandle, -1 /* absolute position */, new FloatWA(3), remoteApi.simx_opmode_discontinue);
        }
    }

    public TreeMap<Double, Vector3d> getPositions() {
        return positions;
    }

    public TreeMap<Double, List<Vector2d>> getErrors() {
        return errors;
    }
}
