from emerge import Controller
from time import sleep
import serial
from pyax12.utils import degrees_to_dxl_angle

class PhantomXController(Controller):
	def __init__(self, chromo, port, baudrate):
		super().__init__(chromo)
		self.conn = serial.Serial(port, baudrate)
		
	def update_joint(self, id, pos):
		target = degrees_to_dxl_angle(pos)
		self.conn.write(str(id) + str(target))
		
	def close(self):
		self.conn.close()


conn = serial.Serial("COM6", 38400)
print("connected")
#print(conn.readline())
conn.write(b'1\n')
conn.write(b'500\n')
conn.write(b'2\n')
conn.write(b'500\n')
sleep(10)
conn.close()
#conn.write(b'250\n')
#while(True):
#	print(conn.read())
