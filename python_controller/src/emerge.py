from pyax12.connection import Connection
from math import sin, pi
import time
import tkinter as tk

class ModuleWindow:
	def __init__(self, ids):
		# Tkinter GUI
		self.root = tk.Tk()
		self.root.geometry("300x500")
		self.root.columnconfigure(0, weight=1)
		self.scales = {}
		self.top = tk.TOP
		row = 0
		for id in ids:
			frame = tk.Frame(self.root)
			frame.grid(row=row, column=0)
			id_label = tk.Label(frame, text="Id: " + str(id))
			id_label.pack(side=tk.LEFT)
		
			scale = tk.Scale(frame, from_=-105.0, to=105.0, digits=5, resolution = 0.01, orient=tk.HORIZONTAL, state=tk.NORMAL, length=400, sliderlength=10)
			scale.pack(fill=tk.BOTH, expand=True, side=tk.LEFT)
			self.scales[id] = scale
			row += 1
		
	def update(self, id, pos):
		self.scales[id].set(pos)
		self.root.update()

class Module:
	def __init__(self, id, amplitude_control, ang_freq_control, phase_control):
		self.id = id
		self.max_amplitude = 0.5 * pi # taken from EmergeModuleSet.java
		self.max_ang_freq = 2.0 # taken from EmergeModuleSet.java
		self.amplitude_control = amplitude_control
		self.ang_freq_control = ang_freq_control
		self.phase_control = phase_control
	
	def get_pos(self, time):
		# Replicating how it is done in modules.control.SinusoidalController#updateJoints
		amplitude = self.max_amplitude * self.amplitude_control
		ang_freq = self.max_ang_freq * self.ang_freq_control
		# BIG NOTE: In the original java code, there is a bug where phaseControl is int, so jvm rounds the division to 0 (if <180) or 1
		#rad = amplitude * sin(ang_freq * time + (1 if self.phase_control >= 180 else 0) * pi)
		rad = amplitude * sin(ang_freq * time + (self.phase_control / 180) * pi)
		# To degrees
		return rad * 180 / pi
		
def chromo_to_modules(chromo):
	assert (len(chromo) + 3) % 9 == 0
	n = int((len(chromo) + 3) / 9)
	modules = []
	for i in range(n):
		if chromo[i] == 1.0:
			amplitude = chromo[n * 4 - 3 + i]
			ang_freq = chromo[n * 5 - 3 + i]
			phase = chromo[n * 6 - 3 + i]
			modules.append(Module(i, amplitude, ang_freq, phase))
	return modules
	
def print_modules(modules):
	headers = ["id", "amp", "ang_freq", "phase"]
	row_format_headers = "{:<5} " + ("{:<10} " * (len(headers) - 1))
	row_format = "{:<5} " + ("{:<10.4f} " * (len(headers) - 1))
	print(row_format_headers.format(*headers))
	for module in modules:
		print(row_format.format(module.id, module.amplitude_control, module.ang_freq_control, module.phase_control))

class Controller:
	def __init__(self, chromo):
		self.chromo = chromo
		self.modules = chromo_to_modules(chromo)
		print("Created ", len(self.modules), " modules")
		print_modules(self.modules)
		
	def update_joint(self, id, pos):
		pass
		
	def close(self):
		pass

	def update_joints(self, time):
		new_pos = []
		for module in self.modules:
			self.update_joint(module.id, module.get_pos(time))
		
	def prep(self):
		self.update_joints(0)
			
	def start(self, timeout=None, skip=None):
		print("Starting walk...")
		start_time = time.time()
		#t = 0
		last_command = 0
		while(True):
			current_time = time.time()
			t = current_time - start_time
			#t += 0.05
			if timeout is not None and t > timeout:
				break
			else:
				since_last_command = current_time - last_command
				if (skip and since_last_command > skip) or not skip:
					self.update_joints(t)
					#print("Time since last command:", since_last_command)
					last_command = current_time
		
		print("Walk done")
		
	def prompt_lifecycle(self, timeout=None, skip=None):
		input("Press to prepare module start positions...")
		self.prep()
		input("Press to start walk...")
		self.start(timeout=timeout, skip=skip)
		self.close()
		
class AX12AController(Controller):
	def __init__(self, chromo, port, ids, speed=200):
		super().__init__(chromo)
		self.conn = Connection(port, baudrate=1000000, waiting_time=0.005)
		self.ids = ids
		self.speed = speed
		for module in self.modules:
			id = ids[module.id]
			print(id, " motor check: ", self.conn.ping(id))
		print("Checks done")
		
	def update_joint(self, id, pos):
		self.conn.goto(self.ids[id], int(pos), speed=self.speed, degrees=True)
		
	def close(self):
		self.conn.close()
		
class PrintingController(Controller):
	def update_joint(self, id, pos):
		print("Moving", id, "to", pos)
		
class WindowController(Controller):
	def __init__(self, chromo):
		super().__init__(chromo)
		self.window = ModuleWindow([m.id for m in self.modules])
	
	def update_joint(self, id, pos):
		self.window.update(id, pos)
		
	def close(self):
		self.window.root.mainloop()

if __name__ == "__main__":
	chromo = [1.0, 1.0, 0, 0, 0, 0.5, 0.5, 0.5, 0.5, 180.0, 90.0, 0.0, 0.0, 0.0, 0.0]
	controller = AX12AController(chromo, port="COM4", ids=[6, 7], speed=500)
	#controller = WindowController(chromo)
	controller.prompt_lifecycle()
            